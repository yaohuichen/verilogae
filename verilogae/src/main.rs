//  * ******************************************************************************************
//  * Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

extern crate clap;
use anyhow::{bail, Error};
use anyhow::{Context, Result};
use dirs::data_dir;
use human_panic::setup_panic;
use humantime::Duration;
use maturin::{BuildContext, BuildOptions, Manylinux, PythonInterpreter};
use mimalloc::MiMalloc;
use std::ffi::OsStr;
use std::fs::{create_dir_all, File};
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::time::Instant;
use toml::Value;
use tracing::{error, info};
use verilogae::{init, run_frontend, CodeGen, CodeGenArgs, CodegenTasks, VerilogAeCall};
use yansi_term::Color::Green;

#[global_allocator]
static GLOBAL: MiMalloc = MiMalloc;

use clap::Clap;
use openvaf_constants::Constants;
use openvaf_data_structures::HashMap;
use openvaf_diagnostics::lints::{Lint, LintLevel, Linter};
use openvaf_diagnostics::{ExpansionPrinter, StandardPrinter};
use openvaf_middle::Mir;
use openvaf_preprocessor::std_path;
use openvaf_session::SourceMap;
use std::str::FromStr;

fn shellexpand_input(raw: &OsStr) -> std::result::Result<PathBuf, String> {
    shellexpand(raw).map_err(|err| {
        print_err(&err);
        "".to_string()
    })
}

fn shellexpand(raw: &OsStr) -> Result<PathBuf> {
    if let Some(utf8_str) = raw.to_str() {
        let root_file: &str = &shellexpand::full(utf8_str)?;
        Path::new(root_file)
            .canonicalize()
            .with_context(|| format!("Failed to open {}", root_file))
    } else {
        let path = Path::new(raw);
        path.canonicalize()
            .with_context(|| format!("Failed to open {}", path.display()))
    }
}

fn builtin_std(verilogae_dir: &Path) -> Result<HashMap<&'static str, PathBuf>> {
    let std_dir = verilogae_dir.join("std");

    info!("Using built in standard library at {}", std_dir.display());

    create_dir_all(&std_dir).with_context(|| format!("Failed to create {}", std_dir.display()))?;

    let constants = std_dir.join("constants.va");

    if !constants.exists() {
        info!("'constants.va' not found downloading...");
        let response = minreq::get(
            "https://www.accellera.org/images/downloads/standards/v-ams/constants_2-4.vams",
        )
        .send()
        .context("Failed to download 'constants.va'")?;
        let mut file = File::create(&constants).context("Failed to create constants.va")?;
        file.write_all(response.as_bytes())
            .context("Failed to write 'constants.va'")?;
    }

    let disciplines = std_dir.join("disciplines.va");

    if !disciplines.exists() {
        info!("'disciplines.va' not found downloading");
        let response = minreq::get(
            "https://www.accellera.org/images/downloads/standards/v-ams/disciplines_2-4.vams",
        )
        .send()
        .context("Failed to download 'disciplines.va'")?;
        let mut file = File::create(&disciplines).context("Failed to create 'disciplines.va'")?;
        file.write_all(response.as_bytes())
            .context("Failed to write 'disciplines.va'")?;
    }

    Ok(std_path(constants, disciplines))
}

fn set_lint_lvl(name: &str, lvl: LintLevel) -> Result<()> {
    if let Some(lint) = Lint::from_name(name) {
        lint.overwrite_lvl_global(lvl);
        Ok(())
    } else {
        bail!("{} is not a registered lint!", name)
    }
}

#[allow(clippy::doc_markdown)]
/// VerilogAE is a compiler for VerilogA focused on compact model parameter extaction
/// It generates native python wheels that allow calculating variables such (*extract*)
/// See dspom.gitlab.io/verilogae for detailed documentation
///
///
///     Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
///     it is subject to the license terms in the LICENSE file found in the top-level directory
///     of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
///     No part of VerilogAE may be copied, modified, propagated, or
///     distributed except according to the terms contained in the LICENSE file.
///
#[derive(Clap, Clone)]
#[clap(name = "verilogae", version = env!("CARGO_PKG_VERSION"))]
enum Opts {
    /// Checks a project for errors and warnings but does not generate a binary (saving lots of time)
    Check {
        #[clap(flatten)]
        frontend_args: OpenVafArgs,
    },

    /// Compiles a project to a python wheel
    Build {
        #[clap(flatten)]
        openvaf_args: OpenVafArgs,
        #[clap(flatten)]
        codegen_args: BuildArgs,
    },

    /// Compiles a project and installs the python wheel using pip (required to be installed)
    Install {
        #[clap(flatten)]
        openvaf_args: OpenVafArgs,
        #[clap(flatten)]
        codegen_args: BuildArgs,
    },
}

impl Opts {
    pub fn run(self) -> Result<()> {
        let vae_dir = data_dir()
            .context("Failed to retrieve data dir")?
            .join("verilogae");

        let start = Instant::now();
        match self {
            Self::Check { frontend_args } => {
                frontend_args.run_compilation(&vae_dir)?;
            }
            Self::Build {
                openvaf_args,
                codegen_args,
            } => {
                let file = openvaf_args
                    .input
                    .file_stem()
                    .with_context(|| format!("{} is not a file", openvaf_args.input.display()))?
                    .to_string_lossy()
                    .into_owned();

                let (mir, functions) = openvaf_args.run_compilation(&vae_dir)?;
                codegen_args.build_wheel(
                    &file,
                    &mir,
                    &functions,
                    &vae_dir,
                    openvaf_args.no_threads,
                    openvaf_args.verify,
                )?;
                openvaf_args.late_diagnostics()?;
            }
            Self::Install {
                openvaf_args,
                codegen_args,
            } => {
                let file = openvaf_args
                    .input
                    .file_stem()
                    .with_context(|| format!("{} is not a file", openvaf_args.input.display()))?
                    .to_string_lossy()
                    .into_owned();

                let (mir, functions) = openvaf_args.run_compilation(&vae_dir)?;
                openvaf_args.late_diagnostics()?;

                let wheels = codegen_args.build_wheel(
                    &file,
                    &mir,
                    &functions,
                    &vae_dir,
                    openvaf_args.no_threads,
                    openvaf_args.verify,
                )?;

                let wheel_file = if let [(wheel_file, _, _)] = wheels.as_slice() {
                    wheel_file
                } else {
                    bail!("verilogae install can only install one wheel as conflicts will occur otherwise.\n\
                    Use the --interpreter argument to specify for which interpreter wheels should be generated\n\
                    (generally you should just use the command you use to call python)")
                };

                let mut command = Command::new("pip");

                if cfg!(feature = "debug") {
                    command.arg("-v");
                } else {
                    command.arg("-qq");
                }

                command
                    .arg("install")
                    .arg("--force-reinstall")
                    .arg(wheel_file)
                    .status()
                    .with_context(|| format!("Failed to install {}", wheel_file.display()))?;
            }
        };

        info!(
            "{} ({})",
            Green.bold().paint("Compilation successful"),
            Duration::from(Instant::now().duration_since(start))
        );

        Ok(())
    }
}

#[derive(Clap, Clone)]
struct OpenVafArgs {
    /// The path to be parsed
    #[clap(parse(try_from_os_str = shellexpand_input))]
    input: PathBuf,

    ///Print the compiler directive (file include/macro) backtrace for error/warning messages
    #[clap(short, long)]
    backtrace: bool,

    ///Verifies all individual stages of the compilation process. This should only be used for bug reports and debugging
    #[clap(long)]
    verify: bool,

    /// Lints that should warn instead of their default behaviour
    #[clap(short = 'W', long)]
    warn: Option<Vec<String>>,

    /// Lints that should be ignored instead of their default behaviour
    #[clap(short = 'A', long)]
    allow: Option<Vec<String>>,

    /// Lints that should cause an error instead of their default behaviour
    #[clap(short = 'D', long)]
    deny: Option<Vec<String>>,

    /// Lints that should cause an error and be impossible to overwrite instead of their default behaviour
    #[clap(short = 'F', long)]
    forbid: Option<Vec<String>>,

    /// The path where the VerilogA standard libary can be found
    /// Downloads and uses the latest version from the offical website otherwise
    #[clap(long = "std",parse(try_from_os_str = shellexpand_input))]
    std_path: Option<PathBuf>,

    #[clap(long = "no-threads")]
    pub no_threads: bool,

    /// Allows specifying the value of the Boltzmann constant that is used to calculate the `$vt` function.
    /// Defaults to the NIST2010 constants
    boltzmann_constant: Option<f64>,

    /// Allows specifying the value of the electron charge that is used to calculate the `$vt` function.
    /// Defaults to the NIST2010 constants
    electron_charge: Option<f64>,
}

impl OpenVafArgs {
    pub fn run_compilation(&self, vae_dir: &Path) -> Result<(Mir<VerilogAeCall>, CodegenTasks)> {
        Constants::set(Constants{
            kb: self.boltzmann_constant,
            q: self.electron_charge,
            disclaimer: Some("The value can be set using command line arguments (use verilogae --help for deails)")
        }).unwrap();

        let root_file = &self.input;

        let file_name = if let Some(default_name) = root_file.file_stem() {
            default_name
        } else {
            bail!("{} is not a file", root_file.display());
        };

        if let Some(allow) = &self.allow {
            for lint in allow {
                set_lint_lvl(&lint, LintLevel::Allow)?
            }
        }

        if let Some(warn) = &self.warn {
            for lint in warn {
                set_lint_lvl(&lint, LintLevel::Warn)?
            }
        }

        if let Some(deny) = &self.deny {
            for lint in deny {
                set_lint_lvl(&lint, LintLevel::Deny)?
            }
        }

        if let Some(forbid) = &self.forbid {
            for lint in forbid {
                set_lint_lvl(&lint, LintLevel::Forbid)?
            }
        }

        let paths = if let Some(std_dir) = &self.std_path {
            info!(
                "Using user provided standard library at {}",
                std_dir.display()
            );

            let constants = std_dir.join("constants.va");
            let disciplines = std_dir.join("disciplines.va");
            std_path(constants, disciplines)
        } else {
            builtin_std(&vae_dir)?
        };

        let (sm, main_file) = SourceMap::new_with_mainfile(&root_file)
            .with_context(|| format!("Failed to open {}", root_file.display()))?;

        let (mir, user_requests) = if self.backtrace {
            run_frontend::<ExpansionPrinter>(sm, main_file, paths).with_context(|| {
                format!("Compilation of {} failed!", file_name.to_string_lossy())
            })?
        } else {
            run_frontend::<StandardPrinter>(sm, main_file, paths).with_context(|| {
                format!("Compilation of {} failed!", file_name.to_string_lossy())
            })?
        };

        let functions =
            user_requests.prepare_codegen(&mir, self.backtrace, self.verify, self.no_threads)?;

        self.late_diagnostics()?;

        Ok((mir, functions))
    }

    pub fn late_diagnostics(&self) -> Result<()> {
        if self.backtrace {
            let warnings =
                Linter::late_user_diagnostics::<ExpansionPrinter>().with_context(|| {
                    format!("Compilation of {} failed!", self.input.to_string_lossy())
                })?;

            eprint!("{}", warnings);
        } else {
            let warnings =
                Linter::late_user_diagnostics::<StandardPrinter>().with_context(|| {
                    format!("Compilation of {} failed!", self.input.to_string_lossy())
                })?;

            eprint!("{}", warnings);
        };

        Ok(())
    }
}

#[derive(Clap, Clone)]
struct BuildArgs {
    /// The authors of the package for the auditwheel (defaults to "").
    #[clap(long)]
    authors: Option<Vec<String>>,

    /// The name of the resulting package (name that shows up when installing using pip)
    #[clap(long)]
    package_name: Option<String>,

    /// The name of the resulting module (name used for imports in python)
    #[clap(long)]
    module_name: Option<String>,

    /// The directory to store the built wheel in. Defaults to the current working directory
    #[clap(long, parse(try_from_os_str = shellexpand_input))]
    out_dir: Option<PathBuf>,
    /// Control the platform tag on linux.
    ///
    /// - `1`: Use the manylinux1 tag and check for compliance
    /// - `1-unchecked`: Use the manylinux1 tag without checking for compliance
    /// - `2010`: Use the manylinux2010 tag and check for compliance
    /// - `2010-unchecked`: Use the manylinux2010 tag without checking for compliance
    /// - `2014`: Use the manylinux2010 tag and check for compliance
    /// - `2014-unchecked`: Use the manylinux2014 tag without checking for compliance
    /// - `off`: Use the native linux tag (off)
    ///
    /// This option is ignored on all non-linux platforms
    #[clap(
    long,
    verbatim_doc_comment,
    possible_values = &["1", "1-unchecked", "2010", "2010-unchecked", "2014", "2014-unchecked", "off"],
    case_insensitive = true,
    default_value = "off"
    )]
    pub manylinux: Manylinux,

    #[clap(short, long, parse(try_from_os_str = shellexpand_input))]
    /// The python versions to build wheels for, given as the names of the
    /// interpreters. Uses autodiscovery if not explicitly set.
    pub interpreter: Option<Vec<PathBuf>>,

    #[clap(flatten)]
    pub cg_args: CodeGenArgs,
}

impl BuildArgs {
    pub fn build_wheel(
        self,
        input_file: &str,
        mir: &Mir<VerilogAeCall>,
        taks: &CodegenTasks,
        vae_dir: &Path,
        nothreads: bool,
        verify: bool,
    ) -> Result<Vec<(PathBuf, String, Option<PythonInterpreter>)>> {
        info!("Configuring code generator");

        let call_dir = std::env::current_dir().context("Failed to access the current directory")?;
        let working_dir = vae_dir.join("out");

        create_dir_all(&working_dir).with_context(|| {
            format!(
                "Failed to create working directory {}",
                working_dir.display()
            )
        })?;

        std::env::set_current_dir(&working_dir).with_context(|| {
            format!(
                "Failed to change directory into working dir {}",
                working_dir.display()
            )
        })?;

        let cg = CodeGen::new(&self.cg_args, taks, mir, working_dir)?;

        info!("Building retrieved functions");

        cg.build_objects(nothreads, verify)?;

        info!("Generating python interface");

        let src_dir = Path::new("src");
        create_dir_all(&src_dir)
            .with_context(|| format!("Failed to create src driectory: {}", src_dir.display()))?;

        let lib_rs = src_dir.join("lib.rs");
        let mut lib_rs = File::create(&lib_rs).context("Failed to create source file")?;
        cg.write_rustcode(
            &mut lib_rs,
            self.module_name.as_deref().unwrap_or(input_file),
        )
        .context("Failed to write rust source files")?;

        #[cfg(debug_assertions)]
        {
            info!("Formatting code");
            Command::new("rustfmt")
                .arg("src/lib.rs")
                .status()
                .context("Failed to run rustfmt")?;
        }

        let build_context = self
            .build_context(input_file, call_dir, cg.link_args())
            .context("Failed to generate build options")?;
        info!("Handing off to rustc");

        // Build using maturin
        build_context.build_wheels()
    }

    fn build_context(
        self,
        input_file: &str,
        call_dir: PathBuf,
        link_args: String,
    ) -> Result<BuildContext> {
        let package_name = self.package_name.unwrap_or_else(|| input_file.to_string());
        let module_name = self.module_name.unwrap_or_else(|| input_file.to_string());

        let cargo_toml = Self::create_toml(package_name, module_name, self.authors);

        {
            let mut cargo_toml_file =
                File::create("Cargo.toml").context("Failed to create 'Cargo.toml'")?;
            cargo_toml_file
                .write_all(cargo_toml.as_bytes())
                .context("Failed to write 'Cargo.toml'")?;
        }

        let interpreter_discovery = self.interpreter.is_none();
        let cpu_arg = format!(
            "-C target-cpu={}",
            self.cg_args.cpu.as_deref().unwrap_or("native")
        );

        let mut rustc_extra_args = if cfg!(debug_assertions) {
            vec![link_args, "--emit=llvm-ir".to_string(), cpu_arg]
        } else {
            vec![String::from("--cap-lints allow"), link_args, cpu_arg]
        };

        if self.cg_args.target_features.is_some() {
            rustc_extra_args.push(format!(
                "-C target-features=\"{}\"",
                self.cg_args.features()
            ))
        }

        let mut build_context = BuildOptions {
            manylinux: self.manylinux,
            interpreter: self.interpreter,
            bindings: Some(String::from("pyo3")),
            manifest_path: "Cargo.toml".into(),
            out: Some(self.out_dir.unwrap_or(call_dir)),
            skip_auditwheel: false,
            target: self.cg_args.triple.map(|t| t.to_string()),
            cargo_extra_args: Vec::new(),
            rustc_extra_args,
        }
        .into_build_context(true, false)?;

        if interpreter_discovery && build_context.interpreter.is_empty() {
            build_context.interpreter.extend(
                PythonInterpreter::check_executables(
                    &[PathBuf::from("python")],
                    &build_context.target,
                    &build_context.bridge,
                )
                .context("Could not find any python interpreter")?,
            );
        };

        Ok(build_context)
    }

    fn create_toml(
        package_name: String,
        module_name: String,
        authors: Option<Vec<String>>,
    ) -> String {
        let mut cargo_toml = toml::toml! {
            [package]
            edition = "2018"
            description = "A python package generated by VerilogAE"

            [lib]
            crate-type = ["cdylib"]

            [dependencies]
            rayon = "1.5"
            numpy = "0.12"
            verilogae_util = {version="0.7.2",path="/home/jarod/Projects/verilogae/verilogae_util"}

            [dependencies.pyo3]
            version = "0.12"
            features = ["extension-module"]
        };

        let dependencies = cargo_toml["dependencies"].as_table_mut().unwrap();
        #[cfg(not(debug_assertions))]
        dependencies.insert(
            "verilogae_util".to_owned(),
            Value::String(env!("CARGO_PKG_VERSION").to_string()),
        );
        #[cfg(debug_assertions)]
        {
            let mut table = toml::map::Map::new();
            table.insert(
                "version".to_string(),
                Value::String(env!("CARGO_PKG_VERSION").to_string()),
            );

            let dir = PathBuf::from_str(env!("CARGO_MANIFEST_DIR")).unwrap();
            table.insert(
                "path".to_string(),
                Value::String(
                    dir.parent()
                        .unwrap()
                        .join("verilogae_util")
                        .to_str()
                        .expect("Only UTF-8 paths are allowed for debugging")
                        .to_owned(),
                ),
            );

            dependencies.insert("verilogae_util".to_owned(), Value::Table(table));
        }

        let package = cargo_toml["package"].as_table_mut().unwrap();
        package.insert("name".to_string(), Value::String(package_name));

        package.insert(
            "version".to_string(),
            Value::String(env!("CARGO_PKG_VERSION").to_string()),
        );

        let authors = if let Some(authors) = authors {
            authors.into_iter().map(Value::String).collect()
        } else {
            vec![Value::String("".to_string())]
        };

        package.insert("authors".to_string(), Value::Array(authors));

        cargo_toml["lib"]
            .as_table_mut()
            .unwrap()
            .insert("name".to_string(), Value::String(module_name));

        toml::to_string(&cargo_toml).expect("Failed to serialize Cargo.toml")
    }
}

fn main() {
    #[cfg(windows)]
    yansi_term::enable_ansi_support();

    #[cfg(not(debug))]
    setup_panic!();

    let session = match init() {
        Ok(s) => s,
        Err(err) => {
            print_err(&err);
            return;
        }
    };

    if let Err(e) = Opts::parse().run() {
        print_err(&e)
    }
    drop(session)
}

fn print_err(e: &Error) {
    let mut errors = e.chain();
    if let Some(first) = errors.next() {
        error! {"{}",first}
    }
    for cause in errors {
        eprintln!("{}", cause);
    }
    std::process::exit(1);
}

---
title: 'Reply to the reviewers for the manuscript JEDS-2020-06-0204-SI entitled: "VerilogAE: An Open Source Verilog-A Compiler for Compact Model Parameter Extraction" '
author:
---
The authors would like to thank all reviewers for the helpful comments and reply to them in this document. 
We are especially thankful for reviewer 2, since his constructive criticism helped us to improve the manuscript further. 
<!--
pandoc verilogAE_review_answers.md -s -o verilogAE_review_answers.pdf --variable urlcolor=cyan 
-->

# Reviewer: 1

> **Congratulations on a very important paper.  
> The VerilogAE software fills a gap in the FOSS tool set for compact modelling and simulation. 
> It will be of great interest to all J-EDS readers interested in compact device modelling. 
> I have no hesitation in recommending the paper for publication.**

Thank you for this supportive comment. 

# Reviewer: 2

> **"For example, a model may depend on a current, which in turn is defined by a model equation." 
> I think what the authors are trying to say is that model equations for a particular feature 
> (for example, impact ionization current) may depend on a current (eg, the drain current), 
> and that current (the drain current) is defined by another model equation.**

We have rewritten this paragraph and hope that it is clearer now. See p.2 c.1, the paragraph begins with "Some model equations may have interdependencies with others".

> **By using a measured drain current as an input to compute the impact ionization current, 
> the equations and parameters that go into the drain current equations are removed from 
> the set of dependencies for the impact ionization current.**
> **I would have found it interesting if the authors had provided such an example: 
> here is the set of parameters that go into computing the impact ionization current, 
> and here is the smaller set that is used when the measured drain current is used.**

There is now a direct example in the manuscript, which should resolve any confusion with regards to 
dependency breaking. We show dependency breaking of the avalanche current and the forward transfer 
current in section IV p.4 c.2. 

> **Even in the introduction, I found myself re-writing the paper to more clearly express the issues. 
> "The difficulty of incorporating Verilog-A models into circuit simulators has been overcome with the 
> help of specialized tools ..." -- it's the difficulty of incorporating compact models into circuit 
> simulators that has been overcome by using Verilog-A (instead of C or Fortran) along with those specialized tools.
> Some mention here about "parameter extraction tools" as being a special type of "circuit simulator" 
> (or that both are types of tool that use compact models) would be helpful, to connect this paragraph 
> (about circuit simulators) to the title and abstract (about parameter extraction).
> "However, before a compact model can accurately reproduce device characteristics, 
> its parameters need to be determined, first." 
> What this sentence means to say is "However, before a compact model can accurately reproduce characteristics 
> of devices in a particular semiconductor process, a set of parameters specific to that device and/or 
> process need to be determined."
> "In general, there are two different kinds of extraction steps. The first kind ..." but then the next paragraph 
> talks about "Another approach" instead of "The second kind ..." 
> The description of the first kind talks about "equivalent circuit element values" which unnecessarily 
> introduces this idea of equivalent circuit elements; isn't it sufficient to talk about the "model outputs"?
> And "convergence" at the end of the paragraph refers (I think) to convergence of the optimization process, 
> rather than convergence of the simulation (though that could also be an issue, if the parameters get unusual values).**

The introduction has been completely rewritten, addressing the reviewer's points.

The difficulty of incorporating Verilog-A models into circuit simulators is overcome 
with ADMS and/or VAPP. This is a technically correct sentence and should not be changed.

It is unfortunate that there is neither a lot of literature on parameter extraction tools 
nor an open source tool  that we could cite. 
We understand that this makes following some of the arguments more difficult.

> **Without familiarity with DMT or ICCAP, it's not clear to me that providing an executable shared library is 
> helpful for accessing equations.**

Every compact model parameter extraction tool - be it DMT/ICCAP or other in-house parameter extraction tool/scripts that one 
finds in industry and academia - needs access to the model equations. Those equations are nowadays defined in the Verilog-A files provided by the model developers.

We have modified the introduction on p.1, c.1/2 to address this point.

> **In the paragraph about VAPP and ADMS (page 1, col 2, lines 37-47), there is mention of "network analysis" 
> which is the only reference to this term; probably "circuit simulation" should be used instead.**

This has been removed.

> **The examples in section V don't really demonstrate anything about the tool.**

This is not correct. 
We demonstrate the following features of the software:

* retrieve of model equations from a Verilog-A file
* generation of derivatives using automatic differentiation
* dependency breaking (updated manuscript)
* example applications for small parameter extraction steps

The "import hl2" statement loads the shared library. Access to the "extractions" attribute are the compiled model equations. 
Without VerilogAE this is technically impossible. 

As recommended by the reviewer, we have added one full diode example section V-A as an attempt to make this more clear (
assuming some familiarity with Python from the reader).
It should also be stressed that the Python interface is not the novel thing about VerilogAE. 
One can easily add interfaces to other programing languages.

The compilation of the Verilog-A code happens by invoking the "verilogae install hl2.va" command. 
This command compiles the Verilog-A source and installs it in Python - as we clearly write in the manuscript on p.4, c.2.

The manuscript is not intended to give an overview of a full extraction flow as described in some of our references [1]. 
The intention is to introduce the architecture and features of Verilog-AE, as stated on p.1,c.1. 

> **For example, in part A, the code sample only appears to show one step out of several mentioned in the text.**

Please see the comment above.

> **The code sample defines a function (fun_cjcx) but then does not appear to use that function in the call to scipy.
> optimize.curve_fit.**

Thank you for pointing out this syntax error in the shown example. This has been corrected.

> **Perhaps it would be better to create a simple model with a few parameters so that it is clear what VerilogAE does. 
> It seems like there is some useful technical content, but I'm not understanding it clearly from this paper.**

For clarifying the application a new section V-A has been added, showing as an example the extraction of parameters for a 
simple diode model.

# Reviewer: 3

> **The paper presents a specialized Verilog-A compiler for the model extraction purposes. 
> The development of the free model parameters extraction software is an important step forward, 
> because it allows to avoid purchasing of the expensive licenses. 
> And therefore this paper could be especially interesting for device modeling specialists from the industry. 
> The presented Verilog-A compiler algorithms are novel and modern software development tools such as Rust 
> programming language are used. 
> But the usage of Rust for software development has a small disadvantage, 
> because the most of /industrial systems are based on RHEL-7/8 distributions. 
> And Rust software deployment for such Linus distributions may be difficult. 
> But this disadvantage is not fatal and this software has a significant practical value and this 
> article could be recommended for publication.**

Thank you very much for this comment on RHEL-7/8. Our in-house parameter extraction system will be 
packaged as a docker container in the future, alongside VAE. We believe that this will overcome such 
limitations. 

# Bibliography

[1] T. Rosenbaum, M. Schroter, A. Pawlak and S.
Lehmann, “Automated transit time and transfer current
extraction for single transistor geometries”, in 2013
IEEE Bipolar/BiCMOS Circuits and Technology Meet-
ing (BCTM), IEEE, 2013, pp. 25–28.

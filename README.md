# VerilogAE
A tool that compiles a Verilog-A file to a native python wheel using the OpenVAF compiler to allow access to internal quantities of compact models.

Features:
- Extract the modelcard of the verilog-A code
- Generate functions for variables marked with `(*retrieve*)` in the verilog-A code.
- View the parameters that are relevant to calculate any `(*retrieve*)` variable.

For install instructions, examples and documentation please take a look at our [homepage](https://dspom.gitlab.io/verilogae/).


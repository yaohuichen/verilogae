//  * ******************************************************************************************
//  * Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::functions::VariableFunction;
use crate::{CodeGenArgs, SimParamKind, VerilogAeCall, VerilogAeInput, VerilogAeUnimplimetedCall};
use anyhow::{bail, Result};
use openvaf_codegen_llvm::inkwell::context::Context;
use openvaf_codegen_llvm::inkwell::module::Linkage;
use openvaf_codegen_llvm::inkwell::passes::{PassManager, PassManagerBuilder};
use openvaf_codegen_llvm::inkwell::support::LLVMString;
use openvaf_codegen_llvm::inkwell::targets::{
    CodeModel, FileType, InitializationConfig, RelocMode, Target, TargetMachine, TargetTriple,
};
use openvaf_codegen_llvm::inkwell::types::{BasicType, BasicTypeEnum};
use openvaf_codegen_llvm::inkwell::values::{BasicValue, BasicValueEnum};
use openvaf_codegen_llvm::inkwell::{AddressSpace, OptimizationLevel};
use openvaf_codegen_llvm::{CallTypeCodeGen, CfgCodegen, Intrinsic, LlvmCodegen, LocalValue};
use openvaf_data_structures::index_vec::IndexSlice;
use openvaf_middle::cfg::START_BLOCK;
use openvaf_middle::{CallArg, CallType, DisciplineAccess, Mir};
use std::convert::TryInto;
use std::iter::repeat;
use std::ops::Range;
use std::path::PathBuf;
use tracing::debug;

impl CodeGenArgs {
    fn triple(&self) -> TargetTriple {
        if let Some(triple) = &self.triple {
            TargetTriple::create(&triple.to_string())
        } else {
            TargetMachine::get_default_triple()
        }
    }

    #[must_use]
    pub fn cpu(&self) -> String {
        self.cpu.as_ref().map_or_else(
            || TargetMachine::get_host_cpu_name().to_string(),
            |cpu| cpu.to_owned(),
        )
    }

    pub fn features(&self) -> String {
        let mut dst = String::new();
        if let Some(features) = &self.target_features {
            let mut iter = features.iter();

            if let Some(first) = iter.next() {
                dst.push_str(&first);
                for feature in iter {
                    dst.push(',');
                    dst.push_str(&feature);
                }
            }
        }

        dst
    }
}

pub(super) struct CodeGenCfg {
    msvc: bool,
    opt_lvl: OptimizationLevel,
    features: String,
    triple: String,
    cpu: String,
}

impl CodeGenCfg {
    pub fn new(args: &CodeGenArgs) -> Result<Self> {
        Target::initialize_all(&InitializationConfig::default());
        let opt_lvl = if args.debug {
            OptimizationLevel::None
        } else {
            OptimizationLevel::Aggressive
        };

        let cpu = args.cpu();
        let triple = args.triple();
        let features = args.features();

        let triple_str = triple.as_str().to_bytes();
        let msvc = &triple_str[triple_str.len() - 4..] == b"msvc";

        let target = Target::from_triple(&triple).unwrap();
        if target
            .create_target_machine(
                &triple,
                &cpu,
                &features,
                opt_lvl,
                RelocMode::PIC,
                CodeModel::Default,
            )
            .is_none()
        {
            let triple = triple.as_str().to_string_lossy();
            bail!(
                "Target {} with cpu {} and features [{}] is not supported!",
                triple,
                cpu,
                features
            )
        }

        Ok(Self {
            msvc,
            opt_lvl,
            features,
            cpu,
            triple: triple.as_str().to_str().unwrap().to_owned(),
        })
    }

    pub fn pass_manager_builder(&self) -> PassManagerBuilder {
        let builder = PassManagerBuilder::create();
        builder.set_optimization_level(self.opt_lvl);
        builder
    }

    pub fn target_machine(&self) -> TargetMachine {
        // TODO find out if this not save to just throw into a sync/send wrapper
        let triple = TargetTriple::create(&self.triple);
        let target = Target::from_triple(&triple).unwrap();
        target
            .create_target_machine(
                &TargetTriple::create(&self.triple),
                &self.cpu,
                &self.features,
                self.opt_lvl,
                RelocMode::PIC,
                CodeModel::Default,
            )
            .unwrap()
    }
}

impl VariableFunction {
    pub(super) fn gen_object(
        &self,
        opt: &CodeGenCfg,
        mir: &Mir<VerilogAeCall>,
        dst: &PathBuf,
        verify: bool,
    ) -> Result<PathBuf, LLVMString> {
        let _span = debug!(
            file = display(&self.ffi_name),
            name = display(self.name),
            "compiling object"
        );

        let ctx = Context::create();
        let builder = opt.pass_manager_builder();
        let target = opt.target_machine();

        let target_data = target.get_target_data();
        let mut ctx = LlvmCodegen::new(mir, &ctx, opt.msvc, &target_data, &self.name.as_str());

        let mut cg = self.create_codegenerator(&mut ctx);

        cg.build_blocks();
        cg.ctx.builder.position_at_end(cg.blocks[self.cfg.end()]);
        let return_val = cg.read_local(self.output);

        cg.ctx.builder.build_return(Some(&return_val));

        let mut name = self.ffi_name.clone();
        name.push_str(".o");
        let path = dst.join(&name);

        if verify {
            cg.ctx.module.verify()?;
        }

        let fpm = PassManager::create(&cg.ctx.module);
        builder.populate_function_pass_manager(&fpm);
        fpm.run_on(&cg.function);

        let mpm = PassManager::create(());
        builder.populate_module_pass_manager(&mpm);
        mpm.run_on(&cg.ctx.module);

        target.write_to_file(&cg.ctx.module, FileType::Object, &path)?;

        #[cfg(debug_assertions)]
        {
            let mut name = self.ffi_name.clone();
            name.push_str(".ll");
            cg.ctx.module.print_to_file(&name)?;
        }

        cg.local_values.clear();

        Ok(path)
    }

    fn create_codegenerator<'lt, 'a, 'c>(
        &'lt self,
        ctx: &'lt mut LlvmCodegen<'a, 'c, VerilogAeCall>,
    ) -> CfgCodegen<'lt, 'a, 'c, ExtractionCodeGen<'lt, 'c>, VerilogAeCall, VerilogAeCall> {
        fn add_homogenous_args<'c>(
            list_len: usize,
            ty: BasicTypeEnum<'c>,
            args: &mut Vec<BasicTypeEnum<'c>>,
        ) -> Range<usize> {
            let res = args.len()..list_len + args.len();
            args.extend(repeat(ty).take(list_len));
            res
        }

        let mut args = Vec::with_capacity(32);
        let voltages = add_homogenous_args(
            self.voltages.len(),
            ctx.real_ty().as_basic_type_enum(),
            &mut args,
        );
        let currents = add_homogenous_args(
            self.currents.len(),
            ctx.real_ty().as_basic_type_enum(),
            &mut args,
        );
        let simparams = add_homogenous_args(
            self.simparams.len() + self.simparams_opt.len(),
            ctx.real_ty().as_basic_type_enum(),
            &mut args,
        );
        let simparams_str = add_homogenous_args(
            self.simparams_str.len(),
            ctx.string_ty().as_basic_type_enum(),
            &mut args,
        );
        let simparams_given = add_homogenous_args(
            self.simparams_opt.len(),
            ctx.bool_ty().as_basic_type_enum(),
            &mut args,
        );
        let ports_connected = add_homogenous_args(
            self.ports_connected.len(),
            ctx.bool_ty().as_basic_type_enum(),
            &mut args,
        );
        let port_flows = add_homogenous_args(
            self.port_flows.len(),
            ctx.real_ty().as_basic_type_enum(),
            &mut args,
        );
        let temperature = if self.temperature {
            let res = args.len();
            args.push(ctx.real_ty().as_basic_type_enum());
            Some(res)
        } else {
            None
        };

        let parameters = args.len()..args.len() + self.parameters.len();
        args.extend(
            self.parameters
                .iter()
                .map(|para| ctx.cabi_parameter_ty(ctx.mir[*para].ty)),
        );

        let inputs: Vec<usize> = self
            .inputs
            .iter()
            .map(|(_, local)| {
                let res = args.len().try_into().unwrap();
                let ty = ctx.cabi_parameter_ty(self.cfg.locals[*local].ty);
                args.push(ty);
                res
            })
            .collect();

        let function = ctx.module.add_function(
            &self.ffi_name,
            ctx.cabi_parameter_ty(self.cfg.locals[self.output].ty)
                .fn_type(&args, false),
            Some(Linkage::External),
        );

        let entry = ctx.context.append_basic_block(function, "entry");

        let cg = ctx.cfg_codegen(&self.cfg, function);
        cg.ctx.builder.position_at_end(entry);

        let mut paras: Vec<_> = function.get_params();

        for (idx, param) in parameters.clone().zip(&self.parameters) {
            let ty = cg.ctx.mir[*param].ty;
            ty.with_info(|info| {
                if !info.dimensions.is_empty() {
                    let ty = cg.ctx.ty_from_info(info);
                    let ptr_ty = ty.ptr_type(AddressSpace::Generic);
                    let ptr = cg.ctx.builder.build_pointer_cast(
                        paras[idx].into_pointer_value(),
                        ptr_ty,
                        "abi_to_local",
                    );
                    paras[idx] = cg.ctx.builder.build_load(ptr, "abi_to_local")
                }
            })
        }

        let param = |indecies: Range<usize>| indecies.map(|n| paras[n]).collect();

        let codegen_data = ExtractionCodeGen {
            extraction: self,
            parameters: param(parameters),
            voltages: param(voltages),
            currents: param(currents),
            port_flows: param(port_flows),
            simparams: param(simparams),
            simparams_str: param(simparams_str),
            simparams_given: param(simparams_given),
            port_connected: param(ports_connected),
            temperature: temperature.map(|n| paras[n]),
        };

        let mut cg = cg.attach_call_type_data(codegen_data);
        cg.alloc_vars_and_branches(|_, _, _| unreachable!());

        for ((_, local), arg) in self.inputs.iter().zip(inputs) {
            let value = cg.cfg.locals[*local].ty.with_info(|info| {
                if info.dimensions.is_empty() {
                    paras[arg]
                } else {
                    let ty = cg.ctx.ty_from_info(info);
                    let ptr_ty = ty.ptr_type(AddressSpace::Generic);
                    let ptr = cg.ctx.builder.build_pointer_cast(
                        paras[arg].into_pointer_value(),
                        ptr_ty,
                        "abi_to_local",
                    );
                    cg.ctx.builder.build_load(ptr, "abi_to_local")
                }
            });
            cg.local_values[*local] = LocalValue::Value(value)
        }

        cg.ctx
            .builder
            .build_unconditional_branch(cg.blocks[START_BLOCK]);

        cg
    }
}

pub struct ExtractionCodeGen<'a, 'c> {
    extraction: &'a VariableFunction,
    parameters: Vec<BasicValueEnum<'c>>,
    voltages: Vec<BasicValueEnum<'c>>,
    currents: Vec<BasicValueEnum<'c>>,
    port_flows: Vec<BasicValueEnum<'c>>,
    simparams: Vec<BasicValueEnum<'c>>,
    simparams_str: Vec<BasicValueEnum<'c>>,
    simparams_given: Vec<BasicValueEnum<'c>>,
    port_connected: Vec<BasicValueEnum<'c>>,
    temperature: Option<BasicValueEnum<'c>>,
}

impl<'a, 'c> ExtractionCodeGen<'a, 'c> {
    fn access<T: PartialEq + Copy>(
        val: T,
        arg: &[BasicValueEnum<'c>],
        position: &[T],
    ) -> BasicValueEnum<'c> {
        *position
            .iter()
            .zip(arg)
            .find(|(x, _)| *x == &val)
            .unwrap()
            .1
    }
}

impl<'lt, 'c> CallTypeCodeGen<'lt, 'c> for VerilogAeCall {
    type CodeGenData = ExtractionCodeGen<'lt, 'c>;

    fn read_input<'a, A: CallType>(
        cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        input: &Self::I,
    ) -> BasicValueEnum<'c> {
        let ctx = &cg.call_type_data;

        match input {
            VerilogAeInput::Parameter(param) => {
                ExtractionCodeGen::access(*param, &ctx.parameters, &ctx.extraction.parameters)
            }

            VerilogAeInput::PortConnected(port) => ExtractionCodeGen::access(
                *port,
                &ctx.port_connected,
                &ctx.extraction.ports_connected,
            ),

            VerilogAeInput::SimParam(para, SimParamKind::Real) => {
                ExtractionCodeGen::access(*para, &ctx.simparams, &ctx.extraction.simparams)
            }

            VerilogAeInput::SimParam(para, SimParamKind::RealOptional) => {
                ExtractionCodeGen::access(
                    *para,
                    &ctx.simparams[ctx.extraction.simparams.len()..],
                    &ctx.extraction.simparams_opt,
                )
            }

            VerilogAeInput::SimParam(para, SimParamKind::String) => {
                ExtractionCodeGen::access(*para, &ctx.simparams_str, &ctx.extraction.simparams_str)
            }

            VerilogAeInput::SimParam(para, SimParamKind::RealOptionalGiven) => {
                ExtractionCodeGen::access(
                    *para,
                    &ctx.simparams_given,
                    &ctx.extraction.simparams_opt,
                )
            }

            VerilogAeInput::BranchAccess(DisciplineAccess::Flow, branch) => {
                ExtractionCodeGen::access(*branch, &ctx.currents, &ctx.extraction.currents)
            }

            VerilogAeInput::BranchAccess(DisciplineAccess::Potential, branch) => {
                ExtractionCodeGen::access(*branch, &ctx.voltages, &ctx.extraction.voltages)
            }

            VerilogAeInput::PortFlow(port) => {
                ExtractionCodeGen::access(*port, &ctx.port_flows, &ctx.extraction.port_flows)
            }

            VerilogAeInput::Temperature => ctx.temperature.unwrap(),
        }
    }

    fn gen_call_rvalue<'a, A: CallType>(
        &self,
        cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        _args: &IndexSlice<CallArg, [BasicValueEnum<'c>]>,
    ) -> BasicValueEnum<'c> {
        match self.kind {
            VerilogAeUnimplimetedCall::Noise => {
                // TODO warn
            }
            VerilogAeUnimplimetedCall::TimeDerivative => {
                // TODO warn
            }
        }
        cg.ctx.real_ty().const_float(0.0).as_basic_value_enum()
    }

    fn gen_call<'a, A: CallType>(
        &self,
        _cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        _args: &IndexSlice<CallArg, [BasicValueEnum<'c>]>,
    ) {
        unreachable!()
    }

    fn gen_limexp<'a, A: CallType>(
        cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        arg: BasicValueEnum<'c>,
    ) -> BasicValueEnum<'c> {
        cg.ctx.build_intrinsic_call(Intrinsic::Exp, &[arg])
    }
}

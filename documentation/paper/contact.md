# Contact information

All authors work at TU Dresden, Chair for Electron Devices and Integrated Circuits.

## Markus Müller

Markus.Mueller3@tu-dresden.de
+4915253414160

## Pascal Kuthe
jarodkuthe@protonmail.com
+491739721610

## Michael Schröter
michael.schroeter@tu-dresden.de
+4935146337687

Introduction
============
Installation
------------

Before installing VerilogAE please ensure you have the following dependencies available:

* Python 3.5+ (on windows you have to make sure its in the path)
* C compiler
    * On Windows: `VS C++ build tools`_
    * On Linux/MacOS cmake and the gcc compiler should be preinstalled or can be easily installed using the package manager
* The rust tool chain (rustup)

Verilogae `provides`_ prebuilt binaries for most package managers and a windows installer for x86_64 systems.
If you experience any issues please read the bug


APT (Debian, Ubuntu, Linux Mint)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Download`_ the ``verilogae.deb`` archive and install it using one of the following commands (older debian derivatives might not have the ``apt`` command)

.. code-block:: bash

    apt install <PATH>
    apt-get install <PATH>

Note that if the downloaded archive is in the current directory you need to use ``./<FILE>``
Furthermore older distributions (Ubuntu LTS 18.04 and older, Debian stretch and older) might have significantly outdated system libaries.
In such cases please use the ``verilogae_libcb28.deb`` archive

RPM (CentOs, Fedora, RedHat, OpenSuse)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


`Download`_ the ``verilogae.rpm`` archive and install it using the appropriate command from the ones below (yast2 for OpenSuse)

.. code-block:: bash

    dnf install <PATH>
    yum install <PATH>
    yast2 -i <PATH>

Note that if the downloaded archive is in the current directory you need to use ``./<FILE>`` for ``dnf`` and ``yum``


Pacman (for example archlinux, manjaro)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`Download`_ the ``verilogae.pkg.tar.zst`` archive and install using the following command

.. code-block:: bash

    pacman -U <File>

Winwdows 10 installer
~~~~~~~~~~~~~~~~~~~~~~

`Download`_ and run the ``verilogae.msi`` installer. Follow the instructions. Note:
We currently have no windows developers, so we would greatly appropriate feedback if you have problems during installation. 
We will be glad to help you fix these.

Mac OSX
~~~~~~~~
The developers of VerilogAE do currently not have access to a Mac so we are not able to build and test packages for Mac.
Please follow the instructions for unsupported platforms

Unsuportted platforms
~~~~~~~~~~~~~~~~~~~~~~~~

VerilogAE only provides prebuilt packages for the most mainstream platforms.
While VerilogAE should work on other platform in theory, it has not been tested there.

For other platforms (32bit computers, Mac and ARM) VerilogAE needs to be compiled manually.
In addition to the usual prerequisites, compiling VerilogAE also requires `CMAKE`_ and the developer libraries for LLVM 10 or higher.
While these are often available trough package managers on linux and mac (homebrew), on windows and old/exotic platforms they might not be.
Compiling LLVM can become involved and as such is only recommended to advanced users.

Compiling VerilogAE itself is quite simple. Only the following command is requires.
Note that compiling verilogae can take a long time.

.. code-block:: bash

    cargo install verilogae --git https://gitlab.com/DSPOM/verilogae



Troubleshooting
~~~~~~~~~~~~~~~~

Windows does not have a package manager which can cause difficulty when installing python:

- Python installed from the windows store can not be found by verilogae. Please install python from the official Python website.

- On windows some dependencies may be missing in the PATH. Please ensure that ``py``/``python`` and ``pip`` work in the terminal before using verilogae. Add them to the path if they do not.

If multiple python versions are installed or verilogae can not find a python interpreter the ``--interpreter`` argument can be used to set the path to the python executable.

- Oftentimes the command you use to start python can be used: If python scripts are started ``python3 script.py`` use ``--interpreter python3``
- If multiple python versions are installed, python creates alias such as ``python3.6``.
    These aliases are **not the path to python** and will not work as an interpreter argument.
    Use the ``which`` command on Unix based systems (Mac Os/Linx) and ``where`` on windows to get the actual path.


Compiling the Examples
------------------------

After completing the installation you can run the examples that are shipped with VerilogAE.
The current examples rely on the compact model `HICUM/L2`_.
Go to the tests/ directory and run

.. code-block:: bash

   verilogae install hl2.va

, which will compile the `HICUM/L2`_ Verilog-A into a python whee and install it using pip.

Check that this worked by running:

.. code-block:: python

    python
    >> import hl2


You can now run the examples provided with VerilogAE, as described in the next section.


Bug reports
------------
If you experience a crash, an error or behaviour that you think is a bug please follow these steps to report an issue:

* Create a new issue `here`_.
* Briefly describe the issue
* rerun the same command as you did previously and add ``--verify --notrhreads`` (note that may lead to a different crash in that case also paste the previous crash message)
* Paste the console output of verilogae into the issue
* If verilogae crashed attach any files mentioned in the console output
* If possible attach the VerilogA source file that was used. If you are not able to share the model publicly you can also create a confidential issue which will only be visisble to maintainers.

For any questions feel free to contact the maintainer at ``dspom@protonmail.com``.

.. _here: https://gitlab.com/DSPOM/verilogae/-/issues



.. _HICUM/L2: https://www.iee.et.tu-dresden.de/iee/eb/hic_new/hic_intro.html
.. _`VS C++ build tools`: https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019
.. _`CMAKE`: https://cmake.org/download/
.. _website: https://www.python.org/downloads/windows/
.. _provides: https://gitlab.com/DSPOM/verilogae/-/releases
.. _Download: https://gitlab.com/DSPOM/verilogae/-/releases
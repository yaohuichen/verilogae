//  * ******************************************************************************************
//  * Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use std::fmt::Debug;
use std::path::PathBuf;

use anyhow::{bail, Context, Result};

pub use crate::functions::VariableFunction;
use crate::hir_lowering::{UserRequests, VariableFunctionFinder};
use clap::Clap;
pub use codegen::CodeGen;
use openvaf_ast_lowering::{lower_ast_userfacing_with_printer, AllowedReferences};
use openvaf_data_structures::index_vec::IndexVec;
use openvaf_data_structures::HashMap;
use openvaf_derivatives::generate_derivatives;
use openvaf_diagnostics::lints::Linter;
use openvaf_diagnostics::{
    DiagnosticSlicePrinter, ExpansionPrinter, MultiDiagnostic, StandardPrinter, UserResult,
};
use openvaf_hir_lowering::lower_hir_userfacing_with_printer;
use openvaf_ir::ids::{BranchId, ParameterId, PortId, SyntaxCtx, VariableId};
use openvaf_ir::{Type, Unknown};
use openvaf_middle::cfg::serde_dump::CfgDump;
use openvaf_middle::cfg::ControlFlowGraph;
use openvaf_middle::const_fold::{ConstantPropagation, DiamondLattice};
use openvaf_middle::osdi_types::ConstVal::Scalar;
use openvaf_middle::osdi_types::SimpleConstVal::Real;
use openvaf_middle::{
    CallArg, CallType, Derivative, DisciplineAccess, InputKind, Local, Mir, OperandData,
};
use openvaf_parser::parse_facing_with_printer;
use openvaf_preprocessor::preprocess_user_facing_with_printer;
use openvaf_session::sourcemap::{FileId, Span, StringLiteral};
use openvaf_session::symbols::Symbol;
use openvaf_session::Session;
use openvaf_session::SourceMap;
use openvaf_transformations::{BuildPDG, Simplify, SimplifyBranches, Verify};
use rayon::prelude::*;
use target_lexicon::Triple;
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::fmt::Layer;
use tracing_subscriber::layer::SubscriberExt;

mod codegen;
mod errors;
mod functions;
mod hir_lowering;
mod lints;

pub struct OperatingPointVariable {
    pub name: Symbol,
    pub desc: StringLiteral,
    pub unit: StringLiteral,
}

impl OperatingPointVariable {
    pub fn new(var: VariableId, mir: &Mir<VerilogAeCall>) -> Self {
        let var = &mir.variables[var];
        Self {
            name: var.ident.name,
            desc: var.desc.unwrap_or(StringLiteral::DUMMY),
            unit: var.unit.unwrap_or(StringLiteral::DUMMY),
        }
    }
}

#[derive(Clap, Clone)]
pub struct CodeGenArgs {
    /// Specifies
    #[clap(long = "target-cpu")]
    pub cpu: Option<String>,
    #[clap(long = "target")]
    pub triple: Option<Triple>,
    #[clap(long)]
    pub debug: bool,
    #[clap(long = "target-features")]
    pub target_features: Option<Vec<String>>,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum SimParamKind {
    Real,
    RealOptional,
    RealOptionalGiven,
    String,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum VerilogAeInput {
    Parameter(ParameterId),
    PortConnected(PortId),
    SimParam(StringLiteral, SimParamKind),
    BranchAccess(DisciplineAccess, BranchId),
    PortFlow(PortId),
    Temperature,
}

impl InputKind for VerilogAeInput {
    fn derivative<C: CallType>(&self, unknown: Unknown, mir: &Mir<C>) -> Derivative<Self> {
        match (self, unknown) {
            (Self::Parameter(x), Unknown::Parameter(y)) if *x == y => Derivative::One,
            (Self::BranchAccess(DisciplineAccess::Flow, x), Unknown::Flow(y)) if *x == y => {
                Derivative::One
            }
            (
                Self::BranchAccess(DisciplineAccess::Potential, branch),
                Unknown::NodePotential(node),
            ) => {
                if mir[*branch].hi == node {
                    Derivative::One
                } else if mir[*branch].lo == node {
                    Derivative::Operand(OperandData::Constant(Scalar(Real(-1.0))))
                } else {
                    Derivative::Zero
                }
            }

            _ => Derivative::Zero,
        }
    }

    fn ty<C: CallType>(&self, mir: &Mir<C>) -> Type {
        match self {
            Self::Parameter(param) => mir[*param].ty,
            Self::BranchAccess(_, _)
            | Self::PortFlow(_)
            | Self::Temperature
            | Self::SimParam(_, SimParamKind::RealOptional)
            | Self::SimParam(_, SimParamKind::Real) => Type::REAL,
            Self::PortConnected(_) | Self::SimParam(_, SimParamKind::RealOptionalGiven) => {
                Type::BOOL
            }
            Self::SimParam(_, SimParamKind::String) => Type::STRING,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum VerilogAeUnimplimetedCall {
    Noise,
    TimeDerivative,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct VerilogAeCall {
    kind: VerilogAeUnimplimetedCall,
    sctx: SyntaxCtx,
    span: Span,
}

impl CallType for VerilogAeCall {
    type I = VerilogAeInput;

    fn const_fold(&self, _call: &[DiamondLattice]) -> DiamondLattice {
        DiamondLattice::NotAConstant
    }

    fn derivative<C: CallType>(
        &self,
        orignal: Local,
        _mir: &Mir<C>,
        _arg_derivative: impl FnMut(CallArg) -> Derivative<Self::I>,
    ) -> Derivative<Self::I> {
        // TODO is it save to assume that the derivative of nosie is always zero (in that case treat that special case properly)
        Derivative::Operand(OperandData::Copy(orignal))
    }
}

pub fn init_env_logger() -> Result<()> {
    let filter = tracing_subscriber::EnvFilter::from_env("VERILOGAE_LOG")
        .add_directive(LevelFilter::INFO.into());
    let layer = Layer::new()
        .with_ansi(true)
        .compact()
        .without_time()
        .with_target(false);

    let subscriber = tracing_subscriber::Registry::default()
        .with(filter)
        .with(layer);

    tracing::subscriber::set_global_default(subscriber).context("Logging setup failed!")?;
    Ok(())
}

pub fn init() -> Result<Session> {
    let session = Session::new();
    init_env_logger()?;
    Ok(session)
}

pub fn run_frontend<P: DiagnosticSlicePrinter>(
    sm: Box<SourceMap>,
    main_file: FileId,
    paths: HashMap<&'static str, PathBuf>,
) -> UserResult<(Mir<VerilogAeCall>, UserRequests), P> {
    let ts = preprocess_user_facing_with_printer(sm, main_file, paths)?;
    let ast = parse_facing_with_printer(ts)?;
    let hir = lower_ast_userfacing_with_printer(ast, |_| AllowedReferences::All)?;
    let diagnostic = Linter::early_user_diagnostics()?;
    eprint!("{}", diagnostic);
    let mut lowering = VariableFunctionFinder::new(&hir);
    let mir = lower_hir_userfacing_with_printer(hir, &mut lowering)?;

    if lowering.errors.is_empty() {
        Ok((mir, lowering.dst))
    } else {
        Err(lowering.errors.user_facing())
    }
}

pub struct CodegenTasks {
    pub functions: Vec<VariableFunction>,
    pub op_vars: Vec<OperatingPointVariable>,
    pub parameter_groups: IndexVec<ParameterId, StringLiteral>,
}

pub const BUG_LINK: &str = "https://gitlab.com/DSPOM/verilogae/-/issues/new";

impl UserRequests {
    pub fn prepare_codegen(
        self,
        mir: &Mir<VerilogAeCall>,
        backtrace: bool,
        verify: bool,
        nothreads: bool,
    ) -> Result<CodegenTasks> {
        let module = if let Some(first) = mir.modules.first() {
            first
        } else {
            bail!("VerilogAE currently only supports single module VerilogA files {")
        };
        let mut cfg = module.analog_cfg.borrow_mut();

        if verify {
            verify_cfg(&mut cfg, mir, "creation")?
        }

        let mut errors = MultiDiagnostic(Vec::new());
        generate_derivatives(&mut cfg, mir, &mut errors);
        if !errors.is_empty() {
            let err = if backtrace {
                errors.user_facing::<ExpansionPrinter>().into()
            } else {
                errors.user_facing::<StandardPrinter>().into()
            };
            return Err(err);
        }

        if verify {
            verify_cfg(&mut cfg, mir, "derivative")?
        }

        cfg.insert_variable_declarations(mir);

        if verify {
            verify_cfg(&mut cfg, mir, "variable_initialization")?
        }

        cfg.run_pass(ConstantPropagation::default());

        if verify {
            verify_cfg(&mut cfg, mir, "constant propagation")?
        }

        cfg.run_pass(SimplifyBranches);
        cfg.run_pass(Simplify);

        if verify {
            verify_cfg(&mut cfg, mir, "cfg-simplification")?
        }

        let locations = cfg.intern_locations();
        let pdg = cfg.run_pass(BuildPDG(&locations));

        let functions: Result<_> = if nothreads {
            self.functions
                .into_iter()
                .map(|attr| {
                    VariableFunction::new(attr.output, &attr.inputs, &cfg, &locations, &pdg, mir)
                })
                .collect()
        } else {
            self.functions
                .into_par_iter()
                .map(|attr| {
                    VariableFunction::new(attr.output, &attr.inputs, &cfg, &locations, &pdg, mir)
                })
                .collect()
        };

        let op_vars = self
            .op_vars
            .into_iter()
            .map(|var| OperatingPointVariable::new(var, mir))
            .collect();

        Ok(CodegenTasks {
            functions: functions?,
            op_vars,
            parameter_groups: self.groups,
        })
    }
}

fn verify_cfg<C: CallType>(
    cfg: &mut ControlFlowGraph<C>,
    mir: &Mir<VerilogAeCall>,
    stage: &'static str,
) -> Result<()> {
    let malformations = cfg.run_pass(Verify(mir));
    if !malformations.is_empty() {
        std::fs::write(
            &"malformed_cfg.yaml",
            serde_yaml::to_string(&CfgDump {
                mir: &mir,
                cfg: &cfg,
                blocks_in_resverse_postorder: false,
            })?,
        )?;
        std::fs::write("malformations.yaml", serde_yaml::to_string(&malformations)?)?;
        bail!("VerilogAE produced an illegal MIR during {}! Please post a bug report containing this error message to {} and append the following files: malformed_cfg.yaml, malformations.yaml", stage, BUG_LINK)
    }
    Ok(())
}

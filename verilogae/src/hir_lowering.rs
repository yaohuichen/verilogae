//  * ******************************************************************************************
//  * Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::errors::AttributeError;
use crate::errors::AttributeError::{
    ExpectedStringLiteralForGroup, ExpectedVariableRefForExtract, UnexpectedArgument,
};
use crate::lints::{EmptyGroupName, NonConstantSimParam, SimParamDefault, StopTasksUnsupported};
use crate::{SimParamKind, VerilogAeCall, VerilogAeInput, VerilogAeUnimplimetedCall};
use openvaf_constants::Constants;
use openvaf_data_structures::index_vec::{index_vec, IndexVec};
use openvaf_data_structures::HashMap;
use openvaf_diagnostics::lints::Linter;
use openvaf_diagnostics::MultiDiagnostic;
use openvaf_hir::Primary;
use openvaf_hir::{Expression, Hir};
use openvaf_hir_lowering::{AttributeCtx, ExpressionLowering, HirFold, HirLowering, LocalCtx};
use openvaf_ir::ids::{
    BranchId, ExpressionId, ParameterId, PortId, StatementId, SyntaxCtx, VariableId,
};
use openvaf_ir::{
    Attribute, NoiseSource, PrintOnFinish, Spanned, StopTaskKind, SystemFunctionCall,
};
use openvaf_middle::cfg::{PhiData, TerminatorKind};
use openvaf_middle::osdi_types::ConstVal::Scalar;
use openvaf_middle::osdi_types::SimpleConstVal::Real;
use openvaf_middle::osdi_types::{ConstVal, SimpleConstVal};
use openvaf_middle::{
    BinOp, DisciplineAccess, Operand, OperandData, ParameterCallType, ParameterInput, RValue,
    RealConstCallType, StmntKind, TyRValue, Type,
};
use openvaf_session::sourcemap::{Span, StringLiteral};
use openvaf_session::symbols::Symbol;

pub struct UserRequests {
    pub functions: Vec<RetrieveAttr>,
    pub op_vars: Vec<VariableId>,
    pub groups: IndexVec<ParameterId, StringLiteral>,
}

pub struct RetrieveAttr {
    pub inputs: Vec<VariableId>,
    pub output: VariableId,
}

pub(crate) struct VariableFunctionFinder {
    pub dst: UserRequests,
    retrieve_attr_name: Symbol,
    op_var_attr_name: Symbol,
    group_attr_name: Symbol,
    pub errors: MultiDiagnostic<AttributeError>,
}

impl VariableFunctionFinder {
    pub fn new(hir: &Hir) -> Self {
        Self {
            dst: UserRequests {
                functions: Vec::with_capacity(32),
                op_vars: Vec::with_capacity(32),
                groups: index_vec![StringLiteral::DUMMY; hir.parameters.len()],
            },
            retrieve_attr_name: Symbol::intern("retrieve"),
            op_var_attr_name: Symbol::intern("op_var"),
            group_attr_name: Symbol::intern("group"),
            errors: MultiDiagnostic(Vec::new()),
        }
    }
}

impl HirLowering for VariableFunctionFinder {
    type AnalogBlockExprLower = VerilogAeCall;

    fn handle_attribute(
        ctx: &mut HirFold<Self>,
        attr: &Attribute,
        src: AttributeCtx,
        _sctx: SyntaxCtx,
    ) {
        match src {
            AttributeCtx::Variable(target)
                if attr.ident.name == ctx.lowering.retrieve_attr_name =>
            {
                let inputs = if let Some(inputs) = attr.value {
                    match ctx.hir.expressions[inputs].contents {
                        Expression::Primary(Primary::VariableReference(var)) => vec![var],
                        Expression::Array(ref exprresion) => exprresion
                            .iter()
                            .filter_map(|e| {
                                if let Expression::Primary(Primary::VariableReference(var)) =
                                    ctx.hir[*e].contents
                                {
                                    Some(var)
                                } else {
                                    ctx.lowering
                                        .errors
                                        .add(ExpectedVariableRefForExtract(ctx.hir[*e].span));
                                    None
                                }
                            })
                            .collect(),
                        _ => {
                            ctx.lowering
                                .errors
                                .add(ExpectedVariableRefForExtract(ctx.hir[inputs].span));
                            return;
                        }
                    }
                } else {
                    Vec::new()
                };

                ctx.lowering.dst.functions.push(RetrieveAttr {
                    inputs,
                    output: target,
                })
            }
            AttributeCtx::Variable(op_var) if attr.ident.name == ctx.lowering.op_var_attr_name => {
                if let Some(val) = attr.value {
                    ctx.lowering
                        .errors
                        .add(UnexpectedArgument(ctx.hir[val].span));
                    return;
                }
                ctx.lowering.dst.op_vars.push(op_var)
            }
            AttributeCtx::Parameter(param) if attr.ident.name == ctx.lowering.group_attr_name => {
                if let Some(val) = attr.value {
                    if let Expression::Primary(Primary::Constant(ConstVal::Scalar(
                        SimpleConstVal::String(name),
                    ))) = ctx.hir[val].contents
                    {
                        ctx.lowering.dst.groups[param] = name
                    } else {
                        ctx.lowering
                            .errors
                            .add(ExpectedStringLiteralForGroup(ctx.hir[val].span));
                    }
                } else {
                    Linter::dispatch_late(
                        Box::new(EmptyGroupName(attr.ident.span)),
                        ctx.hir[param].sctx,
                    )
                }
            }
            _ => (),
        }
        // We don't warn on unused attributes here because verilogae is not the only compiler
    }

    fn handle_statement_attribute<'a, 'h, C: ExpressionLowering>(
        _ctx: &mut LocalCtx<'a, 'h, C, Self>,
        _attr: &Attribute,
        _stmt: StatementId,
        _sctx: SyntaxCtx,
    ) {
        // We don't warn on unused attributes here because verilogae is not the only compiler
    }
}

impl ExpressionLowering for VerilogAeCall {
    fn port_flow<L: HirLowering>(
        _ctx: &mut LocalCtx<Self, L>,
        port: PortId,
        span: Span,
    ) -> Option<RValue<Self>> {
        Some(RValue::Use(Operand::new(
            OperandData::Read(VerilogAeInput::PortFlow(port)),
            span,
        )))
    }

    fn branch_access<L: HirLowering>(
        _ctx: &mut LocalCtx<Self, L>,
        access: DisciplineAccess,
        branch: BranchId,
        span: Span,
    ) -> Option<RValue<Self>> {
        Some(RValue::Use(Operand::new(
            OperandData::Read(VerilogAeInput::BranchAccess(access, branch)),
            span,
        )))
    }

    fn parameter_ref<L: HirLowering>(
        _ctx: &mut LocalCtx<Self, L>,
        param: ParameterId,
        span: Span,
    ) -> Option<RValue<Self>> {
        Some(RValue::Use(Operand::new(
            OperandData::Read(VerilogAeInput::Parameter(param)),
            span,
        )))
    }

    fn time_derivative<L: HirLowering>(
        ctx: &mut LocalCtx<Self, L>,
        _: ExpressionId,
        span: Span,
    ) -> Option<RValue<Self>> {
        let call = Self {
            kind: VerilogAeUnimplimetedCall::TimeDerivative,
            sctx: ctx.fold.sctx,
            span,
        };
        Some(RValue::Call(call, IndexVec::new(), span))
    }

    fn noise<L: HirLowering>(
        ctx: &mut LocalCtx<Self, L>,
        _source: NoiseSource<ExpressionId, ()>,
        _name: Option<ExpressionId>,
        span: Span,
    ) -> Option<RValue<Self>> {
        let call = Self {
            kind: VerilogAeUnimplimetedCall::Noise,
            sctx: ctx.fold.sctx,
            span,
        };
        Some(RValue::Call(call, IndexVec::new(), span))
    }

    fn system_function_call<L: HirLowering>(
        ctx: &mut LocalCtx<Self, L>,
        call: SystemFunctionCall<PortId, ParameterId>,
        span: Span,
    ) -> Option<RValue<Self>> {
        let res = match call {
            SystemFunctionCall::Temperature => RValue::Use(Operand::new(
                OperandData::Read(VerilogAeInput::Temperature),
                span,
            )),
            SystemFunctionCall::PortConnected(port) => RValue::Use(Operand::new(
                OperandData::Read(VerilogAeInput::PortConnected(port)),
                span,
            )),
            SystemFunctionCall::ParameterGiven(_) => RValue::Use(Operand::new(
                OperandData::Constant(Scalar(SimpleConstVal::Bool(true))),
                span,
            )),
            SystemFunctionCall::Simparam(name, default) => {
                if let Some(name) = ctx
                    .fold
                    .lower_string_expression::<RealConstCallType>(name)?
                    .const_eval()
                {
                    let name = if let Scalar(SimpleConstVal::String(name)) = name {
                        name
                    } else {
                        unreachable!()
                    };

                    if let Some(default) = default {
                        let start = ctx.current;
                        ctx.enter_new_block();
                        let default_block_head = ctx.current;
                        let default = ctx.fold_real_rhs(default)?;
                        let default = ctx.assign_temporary(default);
                        let default_block_tail = ctx.current;
                        ctx.enter_new_block();
                        let read_block = ctx.current;
                        let val = ctx.assign_temporary(TyRValue {
                            val: RValue::Use(Operand::new(
                                OperandData::Read(VerilogAeInput::SimParam(
                                    name,
                                    SimParamKind::RealOptional,
                                )),
                                span,
                            )),
                            ty: Type::REAL,
                        });

                        ctx.enter_new_block();
                        ctx.terminate_bb(
                            start,
                            TerminatorKind::Split {
                                condition: RValue::Use(Operand::new(
                                    OperandData::Read(VerilogAeInput::SimParam(
                                        name,
                                        SimParamKind::RealOptionalGiven,
                                    )),
                                    span,
                                )),
                                true_block: read_block,
                                false_block: default_block_head,
                                loop_head: false,
                            },
                        );

                        ctx.terminate_bb(read_block, TerminatorKind::Goto(ctx.current));
                        ctx.terminate_bb(default_block_tail, TerminatorKind::Goto(ctx.current));

                        let dst = ctx.cfg.new_temporary(Type::REAL);

                        let mut sources = HashMap::with_capacity(2);
                        sources.insert(default_block_tail, default);
                        sources.insert(read_block, val);

                        ctx.cfg[ctx.current].phi_statements.push(PhiData {
                            dst,
                            sources,
                            sctx: ctx.fold.sctx,
                        });
                        RValue::Use(Operand::new(OperandData::Copy(dst), span))
                    } else {
                        RValue::Use(Operand::new(
                            OperandData::Read(VerilogAeInput::SimParam(
                                name,
                                SimParamKind::RealOptional,
                            )),
                            span,
                        ))
                    }
                } else {
                    let (kind, default) = if let Some(default) = default {
                        let default = ctx.fold_real(default)?;
                        (SimParamDefault::RealOptional, default)
                    } else {
                        (
                            SimParamDefault::Real,
                            Operand::new(OperandData::Constant(Scalar(Real(0.0))), span),
                        )
                    };

                    Linter::dispatch_late(Box::new(NonConstantSimParam(span, kind)), ctx.fold.sctx);

                    RValue::Use(default)
                }
            }
            SystemFunctionCall::SimparamStr(name) => {
                if let Some(name) = ctx
                    .fold
                    .lower_string_expression::<RealConstCallType>(name)?
                    .const_eval()
                {
                    let name = if let Scalar(SimpleConstVal::String(name)) = name {
                        name
                    } else {
                        unreachable!()
                    };

                    RValue::Use(Operand::new(
                        OperandData::Read(VerilogAeInput::SimParam(name, SimParamKind::String)),
                        span,
                    ))
                } else {
                    Linter::dispatch_late(
                        Box::new(NonConstantSimParam(span, SimParamDefault::String)),
                        ctx.fold.sctx,
                    );
                    RValue::Use(Operand::new(
                        OperandData::Constant(Scalar(SimpleConstVal::String(StringLiteral::DUMMY))),
                        span,
                    ))
                }
            }
            SystemFunctionCall::Vt(temp) => {
                let temp = match temp {
                    Some(temp) => ctx.fold_real(temp)?,
                    None => Operand::new(OperandData::Read(VerilogAeInput::Temperature), span),
                };

                RValue::BinaryOperation(
                    Spanned::new(BinOp::Multiply, span),
                    Operand::new(
                        OperandData::Constant(ConstVal::Scalar(Real(
                            Constants::kb(span) / Constants::q(span),
                        ))),
                        span,
                    ),
                    temp,
                )
            }
        };
        Some(res)
    }

    fn stop_task<L: HirLowering>(
        ctx: &mut LocalCtx<Self, L>,
        _kind: StopTaskKind,
        _finish: PrintOnFinish,
        span: Span,
    ) -> Option<StmntKind<Self>> {
        Linter::dispatch_late(Box::new(StopTasksUnsupported { span }), ctx.fold.sctx);
        None
    }
}

impl From<ParameterCallType> for VerilogAeCall {
    fn from(_: ParameterCallType) -> Self {
        unimplemented!()
    }
}

impl From<ParameterInput> for VerilogAeInput {
    fn from(param: ParameterInput) -> Self {
        Self::Parameter(param.0)
    }
}

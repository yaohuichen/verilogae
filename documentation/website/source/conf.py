# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))



# -- Project information -----------------------------------------------------
import sphinx_rtd_theme

project = 'VerilogAE'
copyright = '2020, Pascal Kuthe'
author = 'Pascal Kuthe'

version = '0.3.1'
release = '0.3.1'

extensions = [
    'sphinx.ext.todo',
    "sphinx_rtd_theme",
    'sphinxcontrib.bibtex',
    'sphinxcontrib.katex',
]

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

html_theme_options = {
    'logo_only': True,
    'display_version': True,
    'style_external_links': False,
    # Toc options
    'collapse_navigation': False,
    'style_nav_header_background': '#e6e6e6',
    'sticky_navigation': False,
    'navigation_depth': 4,
    'includehidden': False,
    'titles_only': False
}


html_theme = "sphinx_rtd_theme"

# Output file base name for HTML help builder.
htmlhelp_basename = 'VerilogAE-Doc'

html_logo = "logo.svg"
html_favicon = "logo_small.svg"

numfig = True

# -- Options for Epub output -------------------------------------------------

# Bibliographic Dublin Core info.
epub_title = project

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#
# epub_identifier = ''

# A unique identification for the text.
#
# epub_uid = ''

# A list of files that should not be packed into the epub file.
epub_exclude_files = ['search.html']


# -- Extension configuration -------------------------------------------------

# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

Home
----

.. image:: https://img.shields.io/badge/license-GPL%203.0-brightgreen

.. image:: https://img.shields.io/badge/python-3.6-blue?logo=python

.. role:: verilog(code)
   :language: verilog

VerilogAE is a compiler for compact model parameter extraction.
It parses Verilog-A compact models and 
generates a python module that allows to access the extracted information.
The following features are supported at the moment.

- human readable diagnostic_ information (e.g. syntax errors)
- Extract the modelcard, including parameter default value and boundaries from verilog-A
- Generate callable functions for variables marked with :verilog:`(*extract*)`.
- View the parameters and branches that are relevant to calculate any :verilog:`(*extract*)` of the variables marked for extraction.
- Break dependencies between model equations by inserting measured values for a sub-model, using the following Syntax: :verilog:`(* extract="input1,input2,.." *)`
- Seamless support for numpy arrays


.. _diagnostic: https://dspom.gitlab.io/OpenVAF/diagnostics.html

This website is intended to give an introduction and overview of this project. 
VerilogAE is still in an early stage of development and follows an open-source philosophy.
If you have any suggestions for improvement, or find bugs please `open an issue`_ . 
You are also welcome to contribute and support the project in an active manner.

.. _`open an issue`: https://gitlab.com/DSPOM/verilogae/-/issues/new



.. toctree::
    :caption: Table of Contents
    :name:
    :maxdepth: 2

    introduction
    examples
    API
    limitations
    zcitations

import pickle
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import hl2
# this example demonstrates how to extract the external base collector junction capacitance with Verilog-AE

## Step 1: Load example data
data_path = os.path.join('tests','test_data','example_meas_data.pckle')
with open(data_path, 'rb') as path:
    df = pickle.load(path)

vbc  = df['cjcx']['vbc']
cjcx = df['cjcx']['cjcx']

#load verilog-AE model
cjcx_model = hl2.functions['CjCx_i'].eval #assuming fbcpar = 0 Cjcx_ii is always 0 so Cjcx = Cjcx_i

modelcard = {name: param.default for (name, param) in hl2.modelcard.items()}

#initial guess
i_0           = np.argmin(np.abs(vbc-0))
initial_guess = [cjcx[i_0]/2  , cjcx[i_0]/2  ,  1, 0.3 , 0.3]

#preparation of data
modelcard.pop('cjcx0')
modelcard.pop('cbcpar')
modelcard.pop('vptcx')
modelcard.pop('vdcx')
modelcard.pop('zcx')

#optimize using Verilog-AE function
def model_cjcx(vbc, cjcx0, cbcpar, vptcx, vdcx, zcx):
    return cjcx_model(temperature=298, voltages={'br_bci' : vbc}, cjcx0=cjcx0, cbcpar=cbcpar, vptcx=vptcx, vdcx=vdcx, zcx=zcx,**modelcard)
popt, _cov = curve_fit(model_cjcx,vbc,cjcx,p0=initial_guess)
#postprocessing

#write back results
modelcard['cjcx0']  = popt[0]
modelcard['cbcpar'] = popt[1]
modelcard['vptcx']  = popt[2]
modelcard['vdcx']   = popt[3]
modelcard['zcx']    = popt[4]

#params = {'text.latex.preamble' : [r'\usepackage{siunitx}', r'\usepackage{amsmath}']}
#plt.rcParams.update(params)
cjcx_display = cjcx_model(temperature=298, voltages={'br_bci':vbc}, **modelcard)
print(vbc)
plt.plot(vbc, cjcx*1e15, label='measurement', linestyle=' ', marker='x', markevery=2)
plt.plot(vbc, cjcx_display*1e15 , label='Verilog-AE', linestyle='-')
plt.ylabel(r'$C_{\mathrm{BCx}}\left( \mathrm{fF} \right)$')
plt.xlabel(r'$V_{\mathrm{BC}} \left( \mathrm{V}  \right)$')
plt.legend()
plt.show()

#tikzplotlib.save(os.path.join('tests', 'test_plots', 'cjcx_extraction.tex'), axis_width=fR'\linewidth', strict=False ,extra_axis_parameters=[r'legend style={font=\tiny}', r'tick label style={font=\tiny}'], textsize=6)
//  * ******************************************************************************************
//  * Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::{SimParamKind, VerilogAeCall, VerilogAeInput};
use anyhow::{bail, Result};
use openvaf_ir::ids::{BranchId, ParameterId, PortId, VariableId};
use openvaf_middle::cfg::serde_dump::CfgDump;
use openvaf_middle::cfg::{ControlFlowGraph, InternedLocations, TerminatorKind};
use openvaf_middle::{
    DisciplineAccess, Local, LocalKind, Mir, OperandData, StmntKind, VariableLocalKind,
};
use openvaf_session::sourcemap::StringLiteral;
use openvaf_session::symbols::Symbol;
use openvaf_transformations::{BackwardSlice, ProgramDependenceGraph, RemoveDeadLocals, Simplify};
use tracing::{debug, warn};

#[derive(Clone)]
pub struct VariableFunction {
    pub cfg: ControlFlowGraph<VerilogAeCall>,
    pub inputs: Vec<(Symbol, Local)>,
    pub output: Local,
    pub voltages: Vec<BranchId>,
    pub currents: Vec<BranchId>,
    pub simparams: Vec<StringLiteral>,
    pub simparams_str: Vec<StringLiteral>,
    pub simparams_opt: Vec<StringLiteral>,
    pub ports_connected: Vec<PortId>,
    pub port_flows: Vec<PortId>,
    pub temperature: bool,
    pub parameters: Vec<ParameterId>,
    pub ffi_name: String,
    pub name: Symbol,
    pub description: StringLiteral,
    pub units: StringLiteral,
}

impl<'a> VariableFunction {
    pub fn new(
        output_var: VariableId,
        input_vars: &[VariableId],
        src_cfg: &'a ControlFlowGraph<VerilogAeCall>,
        locations: &InternedLocations,
        pdg: &ProgramDependenceGraph,
        mir: &Mir<VerilogAeCall>,
    ) -> Result<Self> {
        let ffi_name = format!("__vae_function_{}", output_var);
        debug!(
            variable = display(mir[output_var].ident),
            ffi_name = ffi_name.as_str(),
            "Creating slice"
        );

        let mut cfg = src_cfg.clone();
        let mut output = None;
        let mut inputs = Vec::with_capacity(input_vars.len());
        for (local, decl) in cfg.locals.iter_enumerated() {
            match decl.kind {
                LocalKind::Variable(var, VariableLocalKind::User) if var == output_var => {
                    output = Some(local)
                }
                LocalKind::Variable(ref var, VariableLocalKind::User)
                    if input_vars.contains(var) =>
                {
                    inputs.push((*var, local))
                }
                _ => (),
            }
        }

        let output = if let Some(out) = output {
            out
        } else {
            // TODO error with span
            bail!(
                "Retrieved variable {} is never written to!",
                mir[output_var].ident.name
            )
        };

        cfg.run_pass(
            BackwardSlice::new(&pdg, &locations)
                .assuming_locals(inputs.iter().map(|(_, local)| *local))
                .requiring_local(output),
        );
        cfg.run_pass(Simplify);

        debug!("after simplify before replace");

        #[cfg(debug_assertions)]
        std::fs::write(
            &format!("__vae_function_{}.yaml", output_var),
            serde_yaml::to_string(&CfgDump {
                mir: &mir,
                cfg: &cfg,
                blocks_in_resverse_postorder: false,
            })?,
        )?;

        let replacements = cfg.run_pass(RemoveDeadLocals);
        debug!("after replace");

        let output = replacements[output];
        let inputs: Vec<_> = inputs
            .into_iter()
            .filter_map(|(var, local)| {
                if replacements[local] < cfg.locals.len_idx(){
                    Some((mir[var].ident.name, replacements[local]))
                }else {
                    warn!("Variable '{}' marked for retrieval does not depend on variable '{}' marked as input", mir[output_var].ident,mir[var].ident);
                    None
                }
            })
            .collect();

        for (_, input) in &inputs {
            cfg.locals[*input].kind = LocalKind::Temporary
        }

        #[cfg(debug_assertions)]
        std::fs::write(
            &format!("__vae_function_{}.yaml", output_var),
            serde_yaml::to_string(&CfgDump {
                mir: &mir,
                cfg: &cfg,
                blocks_in_resverse_postorder: false,
            })?,
        )?;

        let mut res = Self {
            cfg,
            inputs,
            output,
            parameters: Vec::with_capacity(32),
            voltages: Vec::with_capacity(8),
            currents: Vec::with_capacity(8),
            simparams: Vec::new(),
            simparams_str: Vec::new(),
            simparams_opt: Vec::new(),
            ports_connected: Vec::new(),
            port_flows: Vec::new(),
            ffi_name,
            temperature: false,
            name: mir[output_var].ident.name,
            description: mir[output_var].desc.unwrap_or(StringLiteral::DUMMY),
            units: mir[output_var].unit.unwrap_or(StringLiteral::DUMMY),
        };

        res.find_dependencies();

        Ok(res)
    }

    fn find_dependencies(&mut self) {
        for block in self.cfg.blocks.iter() {
            let stmnt_operands = block.statements.iter().filter_map(|(stmnt, _)| {
                if let StmntKind::Assignment(_, val) = stmnt {
                    Some(val.operands())
                } else {
                    None
                }
            });

            let terminator_operator =
                if let TerminatorKind::Split { condition, .. } = &block.terminator().kind {
                    Some(condition.operands())
                } else {
                    None
                };

            for op in stmnt_operands.chain(terminator_operator).flatten() {
                if let OperandData::Read(ref input) = op.contents {
                    match input {
                        VerilogAeInput::Parameter(param) => {
                            if !self.parameters.contains(param) {
                                self.parameters.push(*param)
                            }
                        }

                        VerilogAeInput::PortConnected(port) => {
                            if !self.ports_connected.contains(port) {
                                self.ports_connected.push(*port)
                            }
                        }

                        VerilogAeInput::SimParam(name, SimParamKind::Real) => {
                            if !self.simparams.contains(name) {
                                self.simparams.push(*name)
                            }
                        }

                        VerilogAeInput::SimParam(name, SimParamKind::RealOptional)
                        | VerilogAeInput::SimParam(name, SimParamKind::RealOptionalGiven) => {
                            if !self.simparams_opt.contains(name) {
                                self.simparams_opt.push(*name)
                            }
                        }

                        VerilogAeInput::SimParam(name, SimParamKind::String) => {
                            if !self.simparams_str.contains(name) {
                                self.simparams_str.push(*name)
                            }
                        }

                        VerilogAeInput::BranchAccess(DisciplineAccess::Flow, branch) => {
                            if !self.currents.contains(branch) {
                                self.currents.push(*branch)
                            }
                        }

                        VerilogAeInput::BranchAccess(DisciplineAccess::Potential, branch) => {
                            if !self.voltages.contains(branch) {
                                self.voltages.push(*branch)
                            }
                        }

                        VerilogAeInput::PortFlow(port) => {
                            if !self.port_flows.contains(port) {
                                self.port_flows.push(*port)
                            }
                        }
                        VerilogAeInput::Temperature => self.temperature = true,
                    }
                }
            }
        }
    }
}

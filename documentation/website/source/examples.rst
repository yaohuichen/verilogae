.. role:: verilog(code)
   :language: verilog

Examples
========

This section will demonstrate a few examples. Which serve to demonstrate both practical applications of VerilogAE
as well as theoretical capabilities. All examples except the simple diode extraction are performed using the `HICUM/L2`_ :cite:`Schroter2010`
model compiled during the introduction (in tests/hl2.va).


Simple diode extraction
-------------------------------

This example demonstrates the entire process of extracting the parameters of a simple diode model from measurements using Verilogae.
The following Verilog-A file "diode.va" defines this exemplary model.

.. code-block:: Verilog

    `include "constants.vams"
    `include "disciplines.vams"
    module diode_va(A,C);
        inout A, C;
        electrical A,C,CI;
        branch (A,CI) br_a_ci;
        branch (CI,C) br_ci_c;
        //Saturation current [A]
        parameter real Is = 1e-14 from [0:inf];
        //Ohmic res [Ohm]
        parameter real Rs = 0.0 from [0:inf];
        //Emission coef
        parameter real N = 1.0 from [0:inf];
        //Junction capacitance [F]
        parameter real Cjo = 0.0 from [0:inf];
        //Junction potential [v]
        parameter real Vj = 1.0 from [0.2:2];
        //Grading coef
        parameter real M = 0.5 from [0:inf];
        real Vd, Vr, Qd;
        //variables with *extract* will be available in shared library
        (*retrieve*) real Id;
        (*retrieve*) real Cd;
        real VT,x,y,vf;
        analog begin
        VT = `P_K*$temperature /`P_Q;
        Vd = V(br_a_ci);
        Vr = V(br_ci_c);
        Id = Is * (exp(Vd / (N * VT)) - 1);
        //junction capacitance
        //smoothing of voltage over cap
        vf   = Vj*(1 - pow(3, -1/M));
        x    = (vf-Vd)/VT;
        y    = sqrt(x*x + 1.92);
        Vd   = vf-VT*(x + y)/(2);
        Qd   = Cjo*Vj * (1-pow(1-Vd/Vj, 1-M))/(1-M);
        Cd   = ddx(Qd,V(A));
        I(br_a_ci) <+ Id + ddt(Qd);
        I(br_ci_c) <+ Vr / Rs;
    end
    endmodule


To extract the model parameters, the source is first made available to
Python as a shared library using Verilog-AE:

.. code-block:: bash

    verilogae install diode.va

Next, in Python, the necessary modules and some measurement data are loaded:

.. code-block:: python

    from scipy.optimize import curve_fit
    import numpy  as np
    import pandas as pd
    import diode #->VAE compiled shared lib

    # load data
    df = pd.read_pickle(
      'documentation/examples/example_dio_data.pckle')
    Vd = df['V_AC'].to_numpy() #Voltage over Diode
    Id = df['I_A'].to_numpy()  #Anode Current
    Cd = df['C_AC'].to_numpy() #Capacitance over Diode

The imported diode module is the shared library, generated using VerilogAE.
It provides the model equations for the variables that have been marked with (*retrieve*) in the source.
First, the low injection related parameters are extracted:

.. code-block:: python

    #extraction low injection
    def fun_id(vd, N, Is):
        Id = diode.functions['Id'].eval(
            temperature=300,
            voltages={'br_a_ci':vd},
            Is=Is, N=N)
        return np.log10(Id)
    low_inj = np.logical_and(Vd<0.7,Vd>0.4)
    popt, _cov = curve_fit(
        fun_id,Vd[low_inj],np.log10(Id[low_inj]),
        p0=[1,1e-13])
    N,Is = popt

Now one can extract the series resistance of the model:

.. code-block:: python

    # resistance at high injection
    high_inj = Vd>0.7
    def fun_id_high(Vd, R):
        return diode.functions['Id'].eval(
            temperature=300,
            voltages={'br_a_ci':Vd-Id[high_inj]*R},
            Is=Is, N=N)
    popt, _cov = curve_fit(
        fun_id_high,Vd[high_inj],Id[high_inj],
        p0=[1])
    R=popt

Finally, the junction capacitance related parameters are extracted:

.. code-block:: python

    #capacitance parameters
    def fun_C(Vd, Cjo, Vj, M):
        return diode.functions['Cd'].eval(
            temperature=300,
            voltages={'br_a_ci':Vd-Id*R},
            Cjo=Cjo, Vj=Vj, M=M)
    popt, _cov = curve_fit(
        fun_C,Vd,Cd,
        p0=[Cd[0],Vd[np.argmax(Cd)],0.5],
        method='lm')
    Cjo, Vj, M = popt

The results of this exemplary parameter extraction are shown below



.. figure:: diode_i.svg

    Diode current: model with extracted parameters vs measurements

.. figure:: diode_c.svg

    Diode capacity: model with extracted parameters vs measurements

The complete extraction was possible without the need to manually code any equations that
are already defined in the source. For more complicated models this saves
hours of work and many lines of duplicate code that would need to be maintained.


Forward transfer current
------------------------

The forward transfer current is an interesting example because the `HICUM/L2`_ model
calculates it using a newton iteration in the source.
This shows that VerilogAE can even handle such complex cases.

First the extract attribute has to be added to the definition of ``itf`` in the HICUM source.

.. code-block:: Verilog

    (*retrieve*) real itf;

Afterwards the Verilog file needs to be compiled & installed.
Then, the forward transfer current can be calculated in python:


.. code-block:: python

    import hl2
    ...
    # Modelcard is loaded from a file
    # Voltages are extracted from the simulator
    itf = hl2.functions['itf'].eval(
        temperature = 300,
        voltages = {
            # Assuming low injection
            'br_bici'=vbc,
            'br_biei'=vbe,
            }
        **modelcard
    )


.. figure:: itf.svg

    Forward transfer current from `HICUM/L2`_ 2.4.0: VerilogAE vs ADS simulator
    (generated using the tests/test_hicum_transfer_current.py script)

Note that we do not go into detail into what parameters are passed, if you are interested in this have a look at the actual example Python code.

Evaluate ddx Operator using Automatic Differentiation
--------------------------------------------------------

The derivative of the internal base-emitter charge :math:`Q_{\mathrm{jei}}`
was chosen as a demonstration of the symbolic differentiation capabilities of VerilogAE.
The `HICUM/L2`_ source currently calculates :math:`C_{\mathrm{jei}}`, the derivative of this charge,
using a hard coded derivative and can thus be regarded as a suitable reference.

First we need to mark :verilog:`Cjei` for retrieval and add a new variable that contains the automatically
generated derivative.

.. code-block:: Verilog

        (* retrieve *) real Cjei;  // Mark Cjei for extraction

        (* retrieve *) real Qjei_ddV; // Add a new variable for the automatic derivative

Next we add the following to the end of the analog block, so that the derivative is actually calculated.

.. code-block:: Verilog

        Cjei_ddV = ddx(Qjei,V(bi));

Afterwards we can run VerilogAE and the compare the results inside python as follows:

.. code-block:: python

    import hl2

    # node voltages
    vb    = np.linspace(0,1,1001)
    vc    = np.linspace(0,1,1001)
    ve    = np.zeros_like(vb)

    #operating point
    vbe = vb-ve
    vbc = vb-vc

    operating_point = {
            # Assuming low injection
            'br_biei' : vbe,
            'br_bpei' : vbe,

            # Assuming low injection
            'br_bici' : vbc,
            'br_bpci' : vbc,
    }

    temperature = np.linspace(300,600,1001)

    modelcard = {name : param.default for (name,param) in hl2.modelcard.items}

    #set some relevant parameters to get realistic results

    modelcard_vae['cjei0'] = 8.869e-12
    modelcard_vae['ajei'] = 1.65
    modelcard_vae['tnom'] = 26.85
    modelcard_vae['vdei'] = 0.714
    modelcard_vae['vgb'] = 0.91
    modelcard_vae['zei'] = 0.2489

    cjei_manual = hl2.functions['Cjei'].eval(temperature=temperature, voltages = operating_point, **modelcard)
    cjei_auto = hl2.functions['Qjei_ddV'].eval(temperature=temperature, voltages = operating_point, **modelcard)

    ...

.. figure:: cjei_from_qjei.svg

    Comparsion of automatically generated Qjei derivative vs HICUM hand implementation (generated using documentation/examples/cjei_from_qjei.py)


Application to HICUM/L2 Parameter Extraction
----------------------------------------------

Next, the application of VerilogAE is demonstrated for three exemplary extraction steps of increasing complexity.
These extraction steps are all relevant during parameter extraction for the compact model `HICUM/L2`_ :cite:`Schroter2010` in practice.
So that you can easily follow along, a simplified version of the `HICUM/L2`_ equivalent circuit
is depicted for convenience.

These examples are not intended to serve as a reference for parameter extraction.
Instead, they shall demonstrate how much one may achieve with only a few lines of code,
using VerilogAE and a robust numerical optimizer.

.. figure:: hicum_l2_equivalent.svg

    Equivalent circuit of the compact model `HICUM/L2`_ without the substrate, self-heating, noise-correlation and NQS networks.

Base-Collector Junction Capacitance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Under "cold" operating conditions the voltage drop over the resistances and self-heating may be neglected and
the potentials at all nodes of the model are known.
Then, the base-collector capacitance can directly be calculated from measured :math:`\mathbf{\bar{Y}}`-parameters :cite:`Schroter2010`.
Next, Perimeter-over-Area (PoA) separation is applied to find the external and internal parts of this capacitance :cite:`Schroter2010`.
Finally, the model equation for :math:`C_{\mathrm{BCx}}`
may be fitted to the measured, PoA separated external base-collector capacitance.
The main part of the Python code to achieve this is shown below (results shown in :numref:`fig-cjcx_extraction`)

.. code-block:: python

    #.. data preprocessing

    import hl2 #VerilogAE module
    modelcard = {name: param.default for (name, param) in hl2.modelcard.items()} #create modelcard

    #preparation of data

    modelcard.pop('cjcx0')
    modelcard.pop('cbcpar')
    modelcard.pop('vptcx')
    modelcard.pop('vdcx')
    modelcard.pop('zcx')

    # Function for optimizer
    def fun_cjcx(vbc, cjcx0, cbcpar, vptcx, vdcx, zcx):

        #wrapper function for curve_fit.
        #CjCx_ii = 0 using the default parameters. Therefore only CjCx_i is used

        return hl2.functions['CjCx_i'].eval(temperature=298,  voltages = {'br_bci' : vbc},
            cjcx0=cjcx0, cbcpar=cbcpar, vptcx=vptcx,vdcx=vdcx, zcx=zcx,**modelcard)

    #call to optimizer
    popt, _cov = scipy.optimize.curve_fit(model_cjcx,vbc,cjcx,p0=initial_guess)

    #data postprocessing ...

.. _fig-cjcx_extraction:

.. figure:: cjcx_extraction.svg

    HICUM :math:`C_{\mathrm{jcx}}` model (lines) with parameters generated using using VerilogAE compared to PoA separated measurement data (symbols) of a SiGe HBT at :math:`T=298K` .




Base-Emitter Diode Parameters for :math:`R_{\mathrm{th}}` Extraction
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For some extraction steps, artificial model parameters, that is parameters that are only relevant during extraction, are required.
E.g., for the :math:`R_{\mathrm{th}}` extraction method from :cite:`Tran,Pawlak2014` one assumes that the entire
base current is flowing through the diode :math:`I_{\mathrm{jBEi}}`.
The required diode parameters for :cite:`Tran,Pawlak2014` may be extracted using the code snippet below, the results are shown in :numref:`fig-ibei_extraction`.

.. code-block:: python

    #... pre-processing
    import hl2
    modelcard = {name: param.default for (name, param) in hl2.modelcard.items()}

    modelcard.pop('ibeis')
    modelcard.pop('mbei')
    modelcard.pop('vge')
    modelcard.pop('zetabet')
    #optimize using VerilogAE extracted function
    def model_ib(vbe, ibeis, mbei, vge, zetabet):

        ib_model = hl2.functions['ibei'].eval(temperature=298, voltages{'br_biei':vbe},
            ibeis=ibeis, mbei=mbei, vge=vge, zetabet=zetabet, **modelcard)

        return np.log10(ib_model)

    popt, _cov = scipy.optimize.curve_fit(model_ib,vbe,np.log10(ib),p0=initial_guess)
    #post-processing ...


.. _fig-ibei_extraction:

.. figure:: ibei_extraction.svg

    HICUM :math:`I_{\mathrm{jBEi}}` model (lines) with parameters extracted using VerilogAE
    compared to measured base currents (symbols) at :math:`V_{\mathrm{BC}}=0.3V`,
    using the assumptions from :cite:`Tran`.
    The fit has been done for :math:`0.8V <V_{\mathrm{BE}}<0.85V`.



Low Current Transit Time Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The next example shows the extraction of the low current transit time parameters **t0**, **dt0h** and **tbvl**.
One way to extract these is by plotting :math:`1/\left( 2\pi f_{\mathrm{ti}} \right)` vs. :math:`1/g_{\mathrm{mi}}`,
where the subscript :math:`i` denotes quantities of the internal transistor :cite:`T.Rosenbaum2011`.
Then, the linear part of the lines can be extrapolated toward :math:`g_{\mathrm{mi}}\rightarrow \infty`,
which yields the low current transit time for each :math:`V_{\mathrm{BC}}`  as the y-axis intercept.
A simple, yet limited, implementation is given by the code snippet below, the results are shown in :numref:`fig-tau0_extraction`.

.. code-block:: python

    #...pre-processing

    import hl2
    modelcard = {name: param.default for (name, param) in hl2.modelcard.items()}

    #preparation of data
    modelcard.pop('tbvl')
    modelcard.pop('dt0h')
    modelcard.pop('t0')
    #optimize using VerilogAE extracted function
    def model_tau_gmi(gmi_inv, t0, dt0h, tbvl, m):
        t0 = hl2.functions['T_f0'].eval(temperature=298,
            voltages={'br_bici':vbc}, t0=t0,dt0h=dt0h,tbvl=tbvl)
        return t0 + m*gmi_inv

    popt, _cov = scipy.optimize.curve_fit(model_tau_gmi,gmi_inv,tau,p0=initial_guess)

    #post-processing ...

.. _fig-tau0_extraction:

.. figure:: t0_extraction.svg

    Extraction of the low current transit time parameters at :math:`T=298K`. (Symbols are measurments)

.. _HICUM/L2: https://www.iee.et.tu-dresden.de/iee/eb/hic_new/hic_intro.html
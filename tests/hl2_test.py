# test the HiCUM/L2 model with verilog-ae using DMT and the HiCUM/L2 verilog code
# both DMT and HiCUM are proprietary, this test case may only be done if you have access to both of them.
#test case to ensure verilog-ae interface
try:
    import hl2
except ModuleNotFoundError:
    print('Warning: hl2 module from verilog-ae not found')

import numpy as np
import pickle as pickle
import os

#check if DMT is available
dmt_avail = True
path_modelcard_dmt = os.path.join('tests','test_data','modelcard_dmt.pckl')
path_models_dmt = os.path.join('tests','test_data','models_dmt.pckl')
try:
    from DMT.hl2 import McHicum, Hl2Model
except ModuleNotFoundError:
    dmt_avail = False

hl2_model_dmt = Hl2Model('hl2','2.4')


def test_hicum_functions():
    #dmt hand implementation ;)!
    if dmt_avail:
        modelcard_dmt = McHicum(version=2.4, va_file=os.path.join('tests','hl2.va'))
        with open(path_modelcard_dmt, 'wb') as mc_file:
            pickle.dump(modelcard_dmt, mc_file)
    else:
        with open(path_modelcard_dmt, 'rb') as mc_file:
            modelcard_dmt = pickle.load(mc_file)
    #let us define some operating points
    vb    = np.linspace(0,1,1001)
    vc    = np.linspace(0,1,1001)
    ve    = np.zeros_like(vb)
    it    = np.logspace(-5,-1,1001) #available from measurements
    temp  = np.linspace(300,600,1001)

    #prepare modelcard so that we get something back ... values are just to get output, make no sense
    modelcard_dmt.set_values({
        'ibcxs' :1e-10,
        'tbhrec':1e-10,
        'thcs'  :1e-10,
        'ireis' :1e-10,
        't0'    :1e-10,
        'alt0'  :1e-10,
        'kt0'   :1e-10,
    })
    op_def = {
        't_dev':temp,
        'vbc'  : vb-vc,
        'vbe'  : vb-ve,
        'vce'  : vc-ve,
        'it'   : it,
    }

    #dmt outputs
    #out        = call_to_dmt                   ( op_information...,   modelcard as kwargs     )
    if False:
        dmt_models  = {}
        ibcxs_t_dmt = hl2_model_dmt.scale_ibcxs_temp(**op_def , **modelcard_dmt.to_kwargs())
        dmt_models['ibcxs_t_dmt']=ibcxs_t_dmt
        ibcxs_dmt   = hl2_model_dmt.model_ibcx_temp(**op_def  , **modelcard_dmt.to_kwargs())
        dmt_models['ibcxs_dmt']=ibcxs_dmt
        rth_t_dmt   = hl2_model_dmt.model_rth_temp(**op_def   , **modelcard_dmt.to_kwargs())
        dmt_models['rth_t_dmt']=rth_t_dmt
        cjci_dmt    = hl2_model_dmt.model_cjci_temp(**op_def  , **modelcard_dmt.to_kwargs())
        dmt_models['cjci_dmt']=cjci_dmt
        cjei_dmt    = hl2_model_dmt.model_cjei_temp(**op_def  , **modelcard_dmt.to_kwargs())
        dmt_models['cjei_dmt']=cjei_dmt
        ibhrec_dmt  = hl2_model_dmt.model_ibhrec_temp(**op_def, **modelcard_dmt.to_kwargs())
        dmt_models['ibhrec_dmt']=ibhrec_dmt
        ibei_dmt    = hl2_model_dmt.model_ibei_temp(**op_def, **modelcard_dmt.to_kwargs())
        dmt_models['ibei_dmt']=ibei_dmt
        irei_dmt    = hl2_model_dmt.model_irei_temp(**op_def, **modelcard_dmt.to_kwargs())
        dmt_models['irei_dmt']=irei_dmt
        ibep_dmt    = hl2_model_dmt.model_ibep_temp(**op_def, **modelcard_dmt.to_kwargs())
        dmt_models['ibep_dmt']=ibep_dmt
        ick_dmt     = hl2_model_dmt.model_ick_temp(**op_def, **modelcard_dmt.to_kwargs()) #-> important model
        dmt_models['ick_dmt']=ick_dmt
        qjei_dmt    = hl2_model_dmt.model_qjei_temp(**op_def, **modelcard_dmt.to_kwargs()) #-> important model
        dmt_models['qjei_dmt']=qjei_dmt
        tauf0_dmt   = hl2_model_dmt.model_tauf0_temp(**op_def, **modelcard_dmt.to_kwargs()) #-> important model
        dmt_models['tauf0_dmt']=tauf0_dmt
        with open(path_models_dmt, 'wb') as mc_file:
            pickle.dump(dmt_models, mc_file)

    else:
        with open(path_models_dmt, 'rb') as mc_file:
            dmt_models = pickle.load(mc_file)

        locals().update(dmt_models) #load variables from dict



    functions_vae = hl2.functions

    operating_point = {
        'br_biei' : op_def['vbe'],
        'br_bici' : op_def['vbc'],
        'br_bpei' : op_def['vbe'],
        'br_bpci' : op_def['vbc'],
    }

    modelcard_vae = {name: param.default for (name,param) in hl2.modelcard.items()}
    modelcard_vae.update(**{
        'ibcxs' :1e-10,
        'tbhrec':1e-10,
        'thcs'  :1e-10,
        'ireis' :1e-10,
        't0'    :1e-10,
        'alt0'  :1e-10,
        'kt0'   :1e-10,
        'flcomp' : 2.4
    })


    #compiled models
    ibcxs_t_vae = functions_vae['ibcxs_t'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    ijbcx_vae   = functions_vae['ijbcx'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    rth_t_vae   = functions_vae['rth_t'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    cjci_vae    = functions_vae['Cjcit'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    cjei_manual_vae = functions_vae['Cjei'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)
    cjei_vae = functions_vae['Qjei_ddV'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    ibh_rec_vae = functions_vae['ibh_rec'].eval(temperature=temp, voltages=operating_point, itf = op_def['it'], **modelcard_vae)
    iavl_vae    = functions_vae['iavl'].eval(temperature=temp, voltages=operating_point, itf = op_def['it'], **modelcard_vae)

    ibei_vae = functions_vae['ibei'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    irei_vae = functions_vae['irei'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    ibep_vae = functions_vae['ibep'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    ick_vae = functions_vae['ick'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    qjei_vae = functions_vae['Qjei'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)

    tauf0_vae = functions_vae['T_f0'].eval(temperature=temp, voltages=operating_point, **modelcard_vae)


    #check models
    assert np.allclose(ibcxs_t_vae,dmt_models['ibcxs_t_dmt'])
    assert np.allclose(ijbcx_vae,dmt_models['ibcxs_dmt'])
    assert np.allclose(rth_t_vae,dmt_models['rth_t_dmt'])
    assert np.allclose(cjci_vae,dmt_models['cjci_dmt'])
    assert np.allclose(cjei_manual_vae,dmt_models['cjei_dmt'])
    assert np.allclose(cjei_vae,dmt_models['cjei_dmt'])

    assert np.allclose(ibh_rec_vae,dmt_models['ibhrec_dmt'])
    assert np.allclose(ibei_vae,dmt_models['ibei_dmt'])
    assert np.allclose(irei_vae,dmt_models['irei_dmt'])
    assert np.allclose(ibep_vae,dmt_models['ibep_dmt'])
    assert np.allclose(ick_vae,dmt_models['ick_dmt'])
    assert np.allclose(qjei_vae,dmt_models['qjei_dmt'])
    assert np.allclose(tauf0_vae,dmt_models['tauf0_dmt'])

def test_hicum_modelcard():
    #dmt hand implementation ;)!
    if dmt_avail:
        modelcard_dmt = McHicum(version=2.4, va_file=os.path.join('tests','hl2.va'))
        with open(path_modelcard_dmt, 'wb') as mc_file:
            pickle.dump(modelcard_dmt, mc_file)
    else:
        with open(path_modelcard_dmt, 'rb') as mc_file:
            modelcard_dmt = pickle.load(mc_file)
    for para_dmt in modelcard_dmt:
        try:
            param_vae = hl2.modelcard[para_dmt.name]
        except KeyError:
            print('KeyError: Parameter with name "' + para_dmt.name +'" found in DMT implementation and not in VAE.')
            continue

        try:
            assert(para_dmt.value == param_vae.default)

        except AssertionError:
            print('AssertionError: Parameter with name "' + para_dmt.name +'" has different default values in DMT ({}) amd VerilogAE({})'.format(para_dmt.value,param_vae.default))

    try:
        assert(para_dmt.min == param_vae.min)
    except AssertionError:
        print('AssertionError: Parameter with name "' + para_dmt.name +'" has different min values in DMT ({}) amd VerilogAE({})'.format(para_dmt.min,param_vae.min) )

    try:
        assert(para_dmt.max == param_vae.max)
    except AssertionError:
        print('AssertionError: Parameter with name "' + para_dmt.name +'" has different max values in DMT ({}) amd VerilogAE({})'.format(para_dmt.max,param_vae.max))

    try:
        assert(para_dmt.inc_min == param_vae.min_inclusive)
    except AssertionError:
        print('AssertionError: Parameter with name "' + para_dmt.name +'" has different min inclusion in DMT ({}) amd VerilogAE({})'.format(para_dmt.inc_min,param_vae.min_inclusive))

    try:
        assert(para_dmt.inc_max == param_vae.max_inclusive)
    except AssertionError:
        print('AssertionError: Parameter with name "' + para_dmt.name +'" has different min inclusion in DMT ({}) amd VerilogAE({})'.format(para_dmt.inc_max,param_vae.max_inclusive))



test_hicum_modelcard()
test_hicum_functions()

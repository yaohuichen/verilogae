//  * ******************************************************************************************
//  * Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::codegen::object_file::CodeGenCfg;
use crate::codegen::python_interface::VariableFunctionPythonClass;
use crate::codegen::rust_interface::VariableFunctionDefinition;
use crate::{CodeGenArgs, CodegenTasks, VerilogAeCall};
use anyhow::{bail, Result};
use modelcard::modelcard;
use openvaf_middle::Mir;
use proc_macro2::Ident;
use proc_macro2::Span;
use quote::quote;
use rayon::prelude::*;
use std::path::PathBuf;
use std::{io::Write, ops::Deref};

mod modelcard;
mod object_file;
mod python_interface;
mod rust_interface;

pub struct CodeGen<'a> {
    cnfg: CodeGenCfg,
    tasks: &'a CodegenTasks,
    mir: &'a Mir<VerilogAeCall>,
    dst: PathBuf,
}

impl<'a> CodeGen<'a> {
    pub fn new(
        args: &CodeGenArgs,
        tasks: &'a CodegenTasks,
        mir: &'a Mir<VerilogAeCall>,
        dst: PathBuf,
    ) -> Result<Self> {
        if dst.as_os_str().to_str().is_none() {
            bail!("VerilogAE currently can't support non UTF8 paths")
        } else {
            Ok(Self {
                cnfg: CodeGenCfg::new(args)?,
                tasks,
                mir,
                dst,
            })
        }
    }

    pub fn build_objects(&self, nothreads: bool, verify: bool) -> Result<()> {
        if nothreads {
            for f in &self.tasks.functions {
                if let Err(error) = f.gen_object(&self.cnfg, self.mir, &self.dst, verify) {
                    bail!(
                        "Code-Generation of {} ({}) failed: {}",
                        f.name,
                        f.ffi_name,
                        error
                    )
                }
            }
        } else {
            let res: Vec<_> = self
                .tasks
                .functions
                .par_iter()
                .map(|f| {
                    if let Err(error) = f.gen_object(&self.cnfg, self.mir, &self.dst, verify) {
                        bail!(
                            "Code-Generation of {} ({}) failed: {}",
                            f.name,
                            f.ffi_name,
                            error
                        )
                    } else {
                        Ok(())
                    }
                })
                .collect();
            for result in res {
                result?
            }
        }

        Ok(())
    }

    #[must_use]
    pub fn link_args(&self) -> String {
        if self.tasks.functions.is_empty() {
            return String::new();
        }
        let mut dst = "-C link-args=\"".to_string();
        for f in &self.tasks.functions {
            let path = self.dst.join(&f.ffi_name);
            dst.push_str(path.to_str().unwrap());
            dst.push_str(".o ");
        }
        dst.push('\"');
        dst
    }

    pub fn write_rustcode(&self, mut f: impl Write, model_name: &str) -> std::io::Result<()> {
        let model_name = Ident::new(model_name, Span::call_site());
        let extern_functions = self
            .tasks
            .functions
            .iter()
            .map(|x| VariableFunctionDefinition(self.mir, x));
        let function_classes = self
            .tasks
            .functions
            .iter()
            .map(|x| VariableFunctionPythonClass(x, self.mir));

        let class_idents = self
            .tasks
            .functions
            .iter()
            .map(|f| Ident::new(&format!("{}_class", &f.ffi_name), Span::call_site()));

        let modelcard = modelcard(self.mir, &self.tasks.parameter_groups);
        let op_vars = &self.tasks.op_vars;

        let module = self.mir.modules.first().unwrap();
        let _module_name = module.ident.as_str();
        let module_name = _module_name.deref();
        let ports: Vec<_> = module
            .ports
            .clone()
            .map(|port| self.mir[self.mir[port].net].ident.as_str().to_string())
            .collect();

        let rust_interface = quote! (
            #![allow(unused_parens)]
            #![allow(unused_mut)]
            #![allow(nonstandard_style)]

            use std::os::raw::c_char;
            use verilogae_util::*;
            use numpy::*;
            use pyo3::prelude::*;
            use pyo3::types::*;
            use rayon::prelude::*;

            extern "C"{
                #(#extern_functions)*
            }
            #(#function_classes)*

            #[pymodule]
            fn #model_name(py: Python, module: &PyModule) -> PyResult<()> {
                let dict = PyDict::new(py);
                #(
                    module.add_class::<#class_idents>()?;
                    dict.set_item(#class_idents::name,PyCell::new(py,#class_idents{})?)?;
                )*
                module.add("functions",dict)?;

                module.add_class::<NonNumericParameter>()?;
                module.add_class::<RealParameter>()?;
                module.add_class::<IntegerParameter>()?;

                module.add("module_name",#module_name);

                let list = PyList::new(py, &[#(#ports),*]);
                module.add("nodes",list);

                let dict = PyDict::new(py);
                #(#modelcard)*
                module.add("modelcard",dict)?;

                let dict = PyDict::new(py);
                #(#op_vars)*
                module.add("op_vars",dict)?;
                Ok(())
            }
        )
        .to_string();
        f.write_all(rust_interface.as_bytes())?;
        Ok(())
    }
}

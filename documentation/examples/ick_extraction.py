# test the HiCUM/L2 model with verilog-ae using DMT and the HiCUM/L2 verilog code
# both DMT and HiCUM are proprietary, this test case may only be done if you have access to both of them.
#test case to ensure verilog-ae interface
try:
    import hl2
except ModuleNotFoundError:
    print('Warning: hl2 module from verilog-ae not found')

import numpy as np
import matplotlib.pyplot as plt
import pickle as pickle
import os


def plot_comp(x, y_model, y_sim, name, x_label, y_label, y_log=False,comparsion='Simulator'):
    """ Plots the model versus the simulation for visual comparision
    """
    _fig = plt.figure()
    #plt.title(name , fontsize=20)
    plt.xlabel(x_label, fontsize=13)
    plt.ylabel(y_label, fontsize=13)
    plt.plot(x, y_model ,label=r'Verilog-AE',markevery=50,linestyle=' ',marker='x')
    plt.plot(x, y_sim  ,label=comparsion,linestyle='-')
    if y_log:
        plt.yscale('log')
    #plt.legend()


dmt_avail = False
path_modelcard_dmt = os.path.join('tests','test_data','modelcard_dmt.pckl')
path_models_dmt = os.path.join('tests','test_data','models_dmt.pckl')
with open(path_models_dmt, 'rb') as mc_file:
    dmt_models = pickle.load(mc_file)

locals().update(dmt_models) #load variables from dict

#vae outputs
vb    = np.linspace(0,1,1001)
vc    = np.linspace(0,1,1001)
ve    = np.zeros_like(vb)
it    = np.logspace(-5,-1,1001) #available from measurements
temp  = np.linspace(300,600,1001)

operating_point = {
    'br_biei' : vc-ve,
    'br_bici' : vb-vc,
    'br_bpei' : vb-ve,
    'br_bpci' : vb-vc,
}

modelcard_vae = {name: param.default for (name,param) in hl2.modelcard.items()}
modelcard_vae.update(**{
    'ibcxs' :1e-10,
    'tbhrec':1e-10,
    'thcs'  :1e-10,
    'ireis' :1e-10,
    't0'    :1e-10,
    'alt0'  :1e-10,
    'kt0'   :1e-10,
})

ick_vae = hl2.functions['ick'].eval(temperature = temp,voltages=operating_point, **modelcard_vae)

print(ick_vae)
#check models
#assert np.allclose(ick_vae,dmt_models['ick_dmt'])

plot_comp(x=vc-ve,y_model=ick_vae*1e3,y_sim=dmt_models['ick_dmt']*1e3,x_label=r' $V_{\mathrm{CE}}\left( \mathrm{V}  \right)$',y_label=r' $I_{\mathrm{ck}}\left( \mathrm{mA} \right)$',name=r' $C_{\mathrm{jei}}(V_{\mathrm{CE}})$',comparsion=r"Python")
plt.show()

#tikzplotlib.save(os.path.join('tests','test_plots','ick_extraction.tex'),axis_width=fR'\linewidth',strict=False,extra_axis_parameters=[r'legend style={font=\tiny}',r'tick label style={font=\tiny}'],textsize=6)


from scipy.optimize import curve_fit
import numpy  as np 
import pandas as pd
import matplotlib.pyplot as plt
import diode #->VAE compiled shared lib

# load data
df = pd.read_pickle('documentation/examples/example_dio_data.pckle')
Vd = df['V_AC'].to_numpy() #Voltage over Diode
Id = df['I_A'].to_numpy()  #Anode Current 
Cd = df['C_AC'].to_numpy() #Capacitance over Diode

#extraction low injection
def fun_id(vd, N, Is):
    Id = diode.functions['Id'].eval(
        temperature=300,
        voltages={'br_a_ci':vd}, 
        Is=Is, N=N)
    return np.log10(Id)
low_inj = np.logical_and(Vd<0.7,Vd>0.4)
popt, _cov = curve_fit(
    fun_id,Vd[low_inj],np.log10(Id[low_inj]),
    p0=[1,1e-13])
N,Is = popt

# resistance at high injection
high_inj = Vd>0.7
def fun_id_high(Vd, R):
    return diode.functions['Id'].eval(
        temperature=300,
        voltages={'br_a_ci':Vd-Id[high_inj]*R}, 
        Is=Is, N=N)
popt, _cov = curve_fit(
    fun_id_high,Vd[high_inj],Id[high_inj],
    p0=[1])
R=popt

#capacitance parameters
def fun_C(Vd, Cj0, Vj, M):
    return diode.functions['Cd'].eval(
        temperature=300,
        voltages={'br_a_ci':Vd-Id*R}, 
        Cj0=Cj0, Vj=Vj, M=M)

popt, _cov = curve_fit(
    fun_C,Vd,Cd,
    p0=[Cd[0],Vd[np.argmax(Cd)],0.5],
    method='lm')
Cj0, Vj, M = popt


def plot_comp(x, y_data, y_sim, x_label, y_label, y_log=False, comparsion='Measurements'):
    """ Plots the model versus the measurments for visual comparision
    """
    _fig = plt.figure()
    plt.xlabel(x_label, fontsize=13)
    plt.ylabel(y_label, fontsize=13)
    plt.plot(x, y_data, label=comparsion, markevery=7, linestyle=' ', marker='x')
    plt.plot(x, y_sim , label=r'extracted',linestyle='-')
    if y_log:
        plt.yscale('log')
    plt.legend()

Id_extracted = diode.functions['Id'].eval(
    temperature=300,
    voltages={'br_a_ci':Vd-Id*R},
    Is=Is, N=N)

Cd_extracted =  diode.functions['Cd'].eval(
    temperature=300,
    voltages={'br_a_ci':Vd-Id*R},
    Cj0=Cj0, Vj=Vj, M=M)

plot_comp(Vd,Id*1e3,Id_extracted*1e3, r'$V_{\mathrm{AC}} \left( \mathrm{V}  \right)$', r'$I_{\mathrm{A}} \left( \mathrm{mA}  \right)$',True)
plot_comp(Vd,Cd*1e15,Cd_extracted*1e15, r'$V_{\mathrm{AC}} \left( \mathrm{V}  \right)$', r'$C_{\mathrm{A}} \left( \mathrm{fF}  \right)$',False)
plt.show()


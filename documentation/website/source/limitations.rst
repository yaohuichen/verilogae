Limitations
===========
.. role:: verilog(code)
   :language: verilog

VerilogAE is a powerful tool however there are things it can't (yet) do.
Some of these limitations are
on purpose, some will be resolved in later stages of the project.
There are limitations inherent to the approach VerilogAE has taken.
These limitations will be discussed on this page.

VerilogAE is not a circuit simulator
-------------------------------------

VerilogAE extracts the part of the model code necessary to calculate a certain variable
and executes it exactly once in exactly the order the code was written in the Verilog-A file.
To calculate this variable, all node voltages need to be given to VerilogAE.
In contrast, a circuit simulator solves for these voltages.

Automatic Differentiation
-------------------------

VerilogAE calculates derivatives by generating the symbolic derivative for every assignment to a variable that is derived (in accordance with the verilog-ams standard, ref todo). 
This approach works well most of the time, however there are some inherent limitations:

  * Some (mostly integer) mathematical operators do not have a well defined derivative, such as modulus or bitwise operations. OpenVAF will throw an error when trying to calculate the derivative of such an operator.
  * Currently, statements that take the derivative of themselves are not implemented (see the trackingissue_)
  * Symbolic derivatives do not take control flow into account, see below.

.. _trackingissue: https://gitlab.com/DSPOM/OpenVAF/-/issues/11

While the first two limitations are fairly self-explanatory, 
the last limitation can actually cause issues in real code.
Symbolic derivatives are only calculated for each individual statement without any knowledge of the remainder of the program.
This is not an issue if the control flow is independent of the variable that is being derived. 
For example, the following derivative will work:

.. code-block:: Verilog

  if (x == 0) begin
    y = 0;
  end
  else begin
    y = 3*V(foo);
  end
  z = ddx( y,V(foo) )

However the following correct, but weird, way to implement :verilog:`z = ddx(V(foo)**2,V(foo));` would not work.

.. code-block:: Verilog

  y = V(foo)
  if (y == 3)
    y = 9;
  else
    y = y*y;
  z = ddx( y,V(foo) )

Since each statement is evaluated on its own, VerilogAE determines that the derivative of :verilog:`y=9;` equals 0. 
However, in the context of the whole program, it should really be 6. 
Note that this is an inherent limitation of automatic differentiation, not of VerilogAE. 
A good Verilog-A model should not contain such code. 

The reason for this is because the control flow of the program depends on the value of the variable that is being differentiated.
To make it easier to notice such occurrences, a warning `is being added`_ which is displayed in such cases

.. _`is being added`: https://gitlab.com/DSPOM/OpenVAF/-/issues/9

Which derivatives work and which don't is still being actively investigated.
However, we have so far only identified two relevant cases, where derivatives alter the control flow:

* Limiting the value of variable

.. code-block:: Verilog

    if (x > 1000)
      x = 1000;

This pattern always works fine

* Iterative solving algorithms such as a newton iteration

.. code-block:: Verilog

    X = X0;
    while (dX > 0.0001 * X ) begin
      DX = `CALCULATE(X);
      X = X+DX;
    end
    z = ddx( X,V(foo) )

Incorrect values may be produced because the iteration stops when X converges but the derivative of X has not converged, yet. 
This is also an inherent problem when applying automatic differentiation to newton solvers. 
Practical tests have shown that these cases can be easily be avoided by including the derivative in the 
loops`s stop condition as shown below:

.. code-block:: Verilog

    X = X0;
    while (dX > 0.0001 * X || ddx( dX,V(foo) ) > 0.0001 * ddx( X,V(foo) )) begin
      DX = `CALCULATE(X);
      X = X+DX;
    end
    z = ddx( X,V(foo) )

Iterative solving algorithms are still under investigation and a formal proof and automation of the above pattern may be provided in the future_. 
(Such a proof can already be found in mathematical literature).

.. _future: https://gitlab.com/DSPOM/OpenVAF/-/issues/10

Splitting Compiler Directives over Macro or Files Borders
---------------------------------------------------------

The Verilog-AMS standard is vague about defining macros. 
As a consequence, the following is not explicitly forbidden in Verilog-A 
and should produce :math:`8`.

.. code-block:: Verilog

    `define definer(name) `define name

    `definer(abc)(foo) 2*foo

    `abc(4)


VerilogAE does not support patterns like this and there are no plans to add support for this.



Unimplemented Verilog-A Features
------------------------------------
The following Verilog-A features are currently not implemented, 
but the eventual goal is to implement all of these and reach full Verilog-A standard compliance.

System function calls
~~~~~~~~~~~~~~~~~~~~~

The most commonly used SystemFunctionCalls/SystemTasks are supported:

- :verilog:`$temperature` and $vt`:  fully supported
- All mathmatical system function calls such as :verilog:`$cos` are fully supported
- :verilog:`$symparam`, :verilog:`$symparam$str`: fully supported
- :verilog:`$stop`/:verilog:`$finish` will generate an error if used (but can be ignored using the -A flag. See verilogae --help for details). In this case the program simply won't stop when encountering these functions))
- :verilog:`$error`/:verilog:`warn`/:verilog:`$display`/:verilog:`$write` will be ignored by OpenVAF (warnings are generated)
- :verilog:`$param_given` is supported in these sense that verilogae always requires that all parameters be specified explicitly. As such this always returns true (1).
- :verilog:`$port_connected` is supported currently not fully supported by verilogae. This currently always returns true (1).

Support for more system function calls will be added in the future as required.
Please open an issue if you need specific functionality.

Analog Filter
~~~~~~~~~~~~~~~
OpenVAF does currently not support most analog filters such as :verilog:`idt` with the expection of those most commonly used in compact modelling:

- The :verilog:`ddx` is already fully functional
- and :verilog:`ddt` operator is already fully functional in OpenVAF however VerilogAE only impliments the DC case. As such an error will be generated if any extraction depends on a time derivative (which can be ignored in which case 0.0 will be used)
- The :verilog:`white_noise` and :verilog:`flickr_noise` functions are supported by OpenVAF but not by VerilogAE so if any their reuslts are releveant to any retrieved function an error will be generated (which can be ignored in which case 0 will be used).

Module System
~~~~~~~~~~~~~

While OpenVAF can handle multiple modules verilogae can not. Furthermore some more advanced features such as generate blocks or nested modules are not supported by OpenVAF either.



Support will be added as required.
Please open an issue if you need specific functionality.

Arrays
~~~~~~~
While most of the infrastructure is in place arrays are currently unfinished in OpenVAF and using them will cause a crash or an error somewhere.


.. _HICUM/L2: https://www.iee.et.tu-dresden.de/iee/eb/hic_new/hic_intro.html
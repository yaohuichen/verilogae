import numpy  as np
import matplotlib.pyplot as plt
import diode #->VAE compiled shared lib



# voltage between 0.1V and 0.6V
Vd = np.linspace(0.1,0.6,1000)



# current with some example parameters
Id = diode.functions['Id'].eval(
    temperature=300,
    voltages={'br_a_ci': Vd},
    Is=1e-9, N=1)

# plot current

plt.figure()
plt.plot(Vd, Id)
plt.yscale('log')
plt.xlabel(r'$V_{\mathrm{ACi}} \left( \mathrm{V}  \right)$')
plt.ylabel(r'$I_{\mathrm{A}} \left( \mathrm{mA}  \right)$')




# capacity with some example parameters

Cd = diode.functions['Cd'].eval(
    temperature=300,
    voltages={'br_a_ci':Vd},
    Cj0=1e-8, Vj=0.7, M=0.3)

# plot capacity

plt.figure()
plt.plot(Vd, Cd*1e9)
plt.xlabel(r'$V_{\mathrm{ACi}} \left( \mathrm{V}  \right)$')
plt.ylabel(r'$C_{\mathrm{A}} \left( \mathrm{nF}  \right)$')


plt.show()


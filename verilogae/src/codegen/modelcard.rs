//  * ******************************************************************************************
//  * Copyright (c) 2020 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::VerilogAeCall;
use openvaf_data_structures::index_vec::{index_vec, IndexSlice, IndexVec};
use openvaf_ir::ids::ParameterId;
use openvaf_middle::const_fold::{CallResolver, DiamondLattice};
use openvaf_middle::{
    COperand, ConstVal, ParameterCallType, ParameterConstraint, ParameterInput, SimpleConstVal,
};
use openvaf_middle::{CallArg, Mir};
use openvaf_session::sourcemap::StringLiteral;
use openvaf_session::symbols::Ident;
use openvaf_session::with_sourcemap;
use proc_macro2::{Span, TokenStream};
use quote::quote;
use quote::ToTokens;
use std::ops::Deref;

trait Numeric: Sized {
    const CLASS: &'static str;
    type MinInterpolator: ToTokens;
    type MaxInterpolator: ToTokens;

    fn min(val: Option<Self>) -> Self::MinInterpolator;
    fn max(val: Option<Self>) -> Self::MaxInterpolator;
}

struct RealMin(Option<f64>);

impl ToTokens for RealMin {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0 {
            Some(min) if min.is_finite() => min.to_tokens(tokens),
            _ => quote!(f64::NEG_INFINITY).to_tokens(tokens),
        }
    }
}

struct RealMax(Option<f64>);

impl ToTokens for RealMax {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0 {
            Some(max) if max.is_finite() => max.to_tokens(tokens),
            _ => quote!(f64::INFINITY).to_tokens(tokens),
        }
    }
}

impl Numeric for f64 {
    const CLASS: &'static str = "RealParameter";
    type MinInterpolator = RealMin;
    type MaxInterpolator = RealMax;

    fn min(val: Option<Self>) -> Self::MinInterpolator {
        RealMin(val)
    }
    fn max(val: Option<Self>) -> Self::MaxInterpolator {
        RealMax(val)
    }
}

struct IntegerMin(Option<i64>);

impl ToTokens for IntegerMin {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        if let Some(min) = self.0 {
            min.to_tokens(tokens)
        } else {
            quote!(i64::MIN).to_tokens(tokens)
        }
    }
}

struct IntegerMax(Option<i64>);

impl ToTokens for IntegerMax {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        if let Some(max) = self.0 {
            max.to_tokens(tokens)
        } else {
            quote!(i64::MAX).to_tokens(tokens)
        }
    }
}

impl Numeric for i64 {
    const CLASS: &'static str = "IntegerParameter";
    type MinInterpolator = IntegerMin;
    type MaxInterpolator = IntegerMax;

    fn min(val: Option<Self>) -> Self::MinInterpolator {
        IntegerMin(val)
    }
    fn max(val: Option<Self>) -> Self::MaxInterpolator {
        IntegerMax(val)
    }
}

struct NumericParam<T: Numeric> {
    name: Ident,
    description: StringLiteral,
    unit: StringLiteral,
    group: StringLiteral,

    default: T,

    min: Option<T>,
    max: Option<T>,

    min_inclusive: bool,
    max_inclusive: bool,
}

impl<T: Numeric + ToTokens + Copy> ToTokens for NumericParam<T> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let Self {
            name,
            description,
            unit,
            default,
            group,
            min,
            max,
            min_inclusive,
            max_inclusive,
        } = self;
        let class = proc_macro2::Ident::new(T::CLASS, Span::call_site());
        let name_str = name.as_str();
        let name = name_str.deref();
        let min = T::min(*min);
        let max = T::max(*max);
        with_sourcemap(|sm| {
            let unit = unit.raw_contents(sm);
            let description = description.raw_contents(sm);
            let group = group.raw_contents(sm);
            quote!(
                #class {
                    name: #name,
                    description: #description,
                    unit: #unit,
                    group: #group,

                    default: #default,
                    min: #min,
                    max: #max,

                    min_inclusive: #min_inclusive,
                    max_inclusive: #max_inclusive,
                }
            )
            .to_tokens(tokens)
        })
    }
}

#[derive(Debug)]
struct DefaultModelcard(IndexVec<ParameterId, Option<ConstVal>>);

impl CallResolver for DefaultModelcard {
    type C = ParameterCallType;

    fn resolve_call(
        &self,
        _input: &Self::C,
        _args: &IndexSlice<CallArg, [COperand<Self::C>]>,
    ) -> DiamondLattice {
        unreachable!()
    }

    fn resolve_input(&self, input: &ParameterInput) -> DiamondLattice {
        if let Some(val) = &self.0[input.0] {
            DiamondLattice::Val(val.clone())
        } else {
            DiamondLattice::NotAConstant
        }
    }
}

pub fn modelcard<'l>(
    mir: &'l Mir<VerilogAeCall>,
    groups: &'l IndexSlice<ParameterId, [StringLiteral]>,
) -> impl Iterator<Item = impl ToTokens> + 'l {
    let mut mc = DefaultModelcard(index_vec![None; mir.parameters.len()]);
    let mc: Vec<_> = mir
        .parameters
        .iter_enumerated()
        .zip(groups)
        .map(|((id, param), group)| PythonMcParameter::new(id, param, &mut mc, *group))
        .collect();
    mc.into_iter()
}

enum PythonMcParameter {
    Real(NumericParam<f64>),
    Int(NumericParam<i64>),
    Other(NonNumericParam),
}

impl PythonMcParameter {
    pub fn new(
        id: ParameterId,
        param: &openvaf_middle::Parameter,
        default_mc: &mut DefaultModelcard,
        group: StringLiteral,
    ) -> Self {
        // TODO check parameters for wellformedness in HIR
        let default = param
            .default
            .read()
            .const_eval_with_inputs(default_mc)
            .unwrap();
        let name = param.ident;
        let description = param.desc.unwrap_or(StringLiteral::DUMMY);
        let unit = param.unit.unwrap_or(StringLiteral::DUMMY);
        let res = match default.clone() {
            ConstVal::Scalar(SimpleConstVal::Real(default)) => {
                let kind = param.kind.read();
                let ranges = if let ParameterConstraint::Ordered { included, .. } = kind.deref() {
                    included
                } else {
                    unreachable!()
                };

                let mut min_inclusive = false;
                let mut max_inclusive = false;
                let mut min = None;
                let mut max = None;
                for range in ranges {
                    if let ConstVal::Scalar(SimpleConstVal::Real(val)) = range
                        .start
                        .bound
                        .const_eval_with_inputs(default_mc)
                        .unwrap()
                    {
                        match min {
                            None => {
                                min = Some(val);
                                min_inclusive = range.start.inclusive;
                            }
                            Some(x) if x < val => {
                                min = Some(val);
                                min_inclusive = range.start.inclusive;
                            }
                            _ => (),
                        }
                    } else {
                        unreachable!()
                    }

                    if let ConstVal::Scalar(SimpleConstVal::Real(val)) =
                        range.end.bound.const_eval_with_inputs(default_mc).unwrap()
                    {
                        match max {
                            None => {
                                max = Some(val);
                                max_inclusive = range.end.inclusive;
                            }
                            Some(x) if x < val => {
                                max = Some(val);
                                max_inclusive = range.end.inclusive;
                            }
                            _ => (),
                        }
                    } else {
                        unreachable!()
                    }
                }

                Self::Real(NumericParam {
                    name,
                    description,
                    unit,
                    default,
                    group,
                    min,
                    max,
                    min_inclusive,
                    max_inclusive,
                })
            }

            ConstVal::Scalar(SimpleConstVal::Integer(default)) => {
                let kind = param.kind.read();
                let ranges = if let ParameterConstraint::Ordered { included, .. } = kind.deref() {
                    included
                } else {
                    unreachable!()
                };

                let mut min_inclusive = false;
                let mut max_inclusive = false;
                let mut min = None;
                let mut max = None;
                for range in ranges {
                    if let ConstVal::Scalar(SimpleConstVal::Integer(val)) = range
                        .start
                        .bound
                        .const_eval_with_inputs(default_mc)
                        .unwrap()
                    {
                        match min {
                            None => {
                                min = Some(val);
                                min_inclusive = range.start.inclusive;
                            }
                            Some(x) if x < val => {
                                min = Some(val);
                                min_inclusive = range.start.inclusive;
                            }
                            _ => (),
                        }
                    } else {
                        unreachable!()
                    }

                    if let ConstVal::Scalar(SimpleConstVal::Integer(val)) =
                        range.end.bound.const_eval_with_inputs(default_mc).unwrap()
                    {
                        match max {
                            None => {
                                max = Some(val);
                                max_inclusive = range.start.inclusive;
                            }
                            Some(x) if x < val => {
                                max = Some(val);
                                max_inclusive = range.start.inclusive;
                            }
                            _ => (),
                        }
                    } else {
                        unreachable!()
                    }
                }

                Self::Int(NumericParam {
                    name,
                    description,
                    unit,
                    group,
                    default,
                    min,
                    max,
                    min_inclusive,
                    max_inclusive,
                })
            }
            default => Self::Other(NonNumericParam {
                name,
                description,
                unit,
                group,
                default,
            }),
        };

        default_mc.0[id] = Some(default);
        res
    }
}

impl ToTokens for PythonMcParameter {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self {
            Self::Real(x) => {
                let name_str = x.name.as_str();
                let name = name_str.deref();
                quote!(dict.set_item(#name,PyCell::new(py,#x)?)?;).to_tokens(tokens);
            }
            Self::Int(x) => {
                let name_str = x.name.as_str();
                let name = name_str.deref();
                quote!(dict.set_item(#name,PyCell::new(py,#x)?)?;).to_tokens(tokens);
            }

            Self::Other(x) => {
                let name_str = x.name.as_str();
                let name = name_str.deref();
                quote!(dict.set_item(#name,PyCell::new(py,#x)?)?;).to_tokens(tokens);
            }
        }
    }
}

struct NonNumericParam {
    name: Ident,
    description: StringLiteral,
    unit: StringLiteral,
    group: StringLiteral,
    default: ConstVal,
}
impl ToTokens for NonNumericParam {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let Self {
            name,
            description,
            unit,
            group,
            default,
        } = self;
        let name_str = name.as_str();
        let name = name_str.deref();
        let default = PythonConstant(&default);
        with_sourcemap(|sm| {
            let unit = unit.raw_contents(sm);
            let description = description.raw_contents(sm);
            let group = group.raw_contents(sm);
            quote!(
                NonNumericParameter {
                    name: #name,
                    description: #description,
                    unit: #unit,
                    group: #group,
                    default: #default.to_object(py),
                }
            )
            .to_tokens(tokens)
        });
    }
}

struct PythonConstant<'a>(&'a ConstVal);

impl<'a> ToTokens for PythonConstant<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0 {
            ConstVal::Scalar(x) => SimplePythonConstant(*x).to_tokens(tokens),
            ConstVal::Array(elements, ty) => ty.with_info(|info| {
                let arr = PythonArrConstant {
                    dimensions: &info.dimensions,
                    elements: &elements,
                };
                quote!(PyList::new(py,#arr)).to_tokens(tokens)
            }),
        }
    }
}

struct SimplePythonConstant(SimpleConstVal);

impl ToTokens for SimplePythonConstant {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0 {
            SimpleConstVal::Real(val) => val.to_tokens(tokens),
            SimpleConstVal::Integer(val) => val.to_tokens(tokens),
            SimpleConstVal::Bool(val) => val.to_tokens(tokens),
            SimpleConstVal::String(val) => val.with_raw_contents(|x| x.to_tokens(tokens)),
        }
    }
}

struct PythonArrConstant<'a> {
    dimensions: &'a [u32],
    elements: &'a [SimpleConstVal],
}

impl<'a> ToTokens for PythonArrConstant<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let mut dimensions = self.dimensions.iter().rev().copied();
        let base_len = dimensions.next().unwrap();
        let elements: Vec<_> = self
            .elements
            .chunks(base_len as usize)
            .map(|x| {
                let vals = x.iter().map(|x| SimplePythonConstant(*x));
                quote!([#(#vals),*])
            })
            .collect();
        let elements = dimensions.fold(elements, |elements, len| {
            elements
                .chunks(len as usize)
                .map(|elements| quote!([#(#elements),*]))
                .collect()
        });
        debug_assert_eq!(elements.len(), 1);
        elements[0].to_tokens(tokens)
    }
}

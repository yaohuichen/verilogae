import pickle
import os
import matplotlib.pyplot as plt
import numpy as np
import hl2
from scipy.optimize import curve_fit


# this example demonstrates how to extract the external base collector junction capacitance with Verilog-AE

## Step 1: Load example data
data_path = os.path.join('tests','test_data','example_meas_data.pckle')
with open(data_path, 'rb') as path:
    df = pickle.load(path)

data_298  = df['ibe']['298']['vbcf_0.3']
data_323  = df['ibe']['323']['vbcf_0.3']
data_348  = df['ibe']['348']['vbcf_0.3']
data_all  = [data_298, data_323, data_348]
temps     = [298,323,348]

#convert to 1D array (needed for curve_fit) with x=vbe,temp y= ib
vbe_all, ib_all, temp_all = [], [], []
for data, temp in zip(data_all,temps):
    filter_ = np.logical_and(0.8<data['vbe'], 0.85>data['vbe'])
    vbe_all.append(data['vbe'][filter_])
    ib_all.append(data['ib'][filter_])
    temp_all.append(np.ones_like(data['vbe'][filter_])*temp)

vbe  = np.hstack(vbe_all)
ib   = np.hstack(ib_all)
temp = np.hstack(temp_all)

#load verilog-AE model
ibei_model = hl2.functions['ibei'].eval
modelcard = {name: param.default for (name, param) in hl2.modelcard.items()}
modelcard.pop('ibeis')
modelcard.pop('mbei')
modelcard.pop('vge')
modelcard.pop('zetabet')

#initial guess
initial_guess = [ib[1]/np.exp(vbe[1]/25e-3)   , 1  ,  1  , 0  ]
lower_bounds  = [ib[1]/np.exp(vbe[1]/25e-3)/10, 0.1,  0.1, -10]
upper_bounds  = [ib[1]/np.exp(vbe[1]/25e-3)*10, 2  ,  2  , +10]
bounds        = (lower_bounds, upper_bounds)
#... pre-processing
#Verilog-AE import here
#optimize using Verilog-AE function
def model_ib(vbe, ibeis, mbei, vge, zetabet):
    ib_model = ibei_model(temperature=temp, voltages = {'br_biei' : vbe}, ibeis=ibeis, mbei=mbei, vge=vge, zetabet=zetabet, **modelcard)
    return np.log10(ib_model)
popt, _cov = curve_fit(model_ib,vbe,np.log10(ib),p0=initial_guess)
#post-processing ...

#write back results
modelcard['ibeis']   = popt[0]
modelcard['mbei']    = popt[1]
modelcard['vge']     = popt[2]
modelcard['zetabet'] = popt[3]

#redefine methode with t_dev
def model_ib(vbe, t_dev, **kwargs):
    ib_model = ibei_model(temperature=t_dev, voltages={'br_biei':vbe}, **kwargs)
    return np.log10(ib_model)

params = {'text.latex.preamble' : [r'\usepackage{siunitx}', r'\usepackage{amsmath}']}
plt.rcParams.update(params)
for data,temp,marker,color in zip(data_all,temps,['x','s','o'],['k','c','g']):
    vbe   = data['vbe']
    ib    = data['ib']
    plt.plot(vbe,ib*1e3,label=r'$T={0:1.1f}$'.format(temp),linestyle=' ',marker=marker, color=color, markevery=3)
    plt.plot(vbe,10**model_ib(vbe,t_dev=temp,**modelcard)*1e3,linestyle='-', color=color)

#plt.plot(vbc,model.model_cjcx_temp(vbc,t_dev=298,**modelcard)*1e15,label='Verilog-AE',linestyle='-')
plt.ylabel(r'\tiny $I_{\mathrm{B}}\left( \mathrm{mA} \right) $')
plt.xlabel(r'\tiny  $V_{\mathrm{BE}} \left( \mathrm{V}  \right) $')
plt.legend()
plt.yscale('log')
plt.show()

#tikzplotlib.save(os.path.join('tests','test_plots','ib_extraction.tex'),axis_width=fR'\linewidth',strict=False,extra_axis_parameters=[r'legend style={font=\tiny}',r'tick label style={font=\tiny}'],textsize=6, figureheight='1.8in', figurewidth='1.9in' )